const fs = require('fs');
const path = require('path');
const schemaCompiler = require('json-schema-to-typescript');
const sass = require('node-sass');

module.exports = function (grunt) {
    'use strict';

    grunt.initConfig({
        copy: {
            build: {
                files: [
                    {
                        expand: true,
                        cwd: './public',
                        src: ['**', '!**/*.sass', '!**/*.ts'],
                        dest: './dist/public'
                    },
                    {
                        expand: true,
                        cwd: './views',
                        src: ['**'],
                        dest: './dist/views'
                    },
                    {
                        expand: true,
                        cwd: './schemas',
                        src: ['**'],
                        dest: './dist/schemas'
                    },
                    {
                        expand: true,
                        cwd: './config',
                        src: ['**'],
                        dest: './dist/config'
                    }
                ]
            }
        },
        ts: {
            public: {
                files: [
                    {
                        src: ['src/public/**/*.ts'],
                        dest: './dist/public/scripts'
                    }
                ],
                options: {
                    target: 'es6',
                    sourceMap: false,
                    rootDir: 'src/public'
                }
            },
            server: {
                files: [
                    {
                        src: ['src/**/*.ts', '!src/.baseDir.ts', '!src/public/**/*.ts'],
                        dest: './dist'
                    }
                ],
                options: {
                    module: 'commonjs',
                    target: 'es6',
                    sourceMap: false,
                    rootDir: 'src',
                    experimentalDecorators: true,
                }
            }
        },
        sass: {
          options: {
              implementation: sass,
              sourceMap: true
          },
          dist: {
              files: [
                  {
                      expand: true,
                      cwd: 'public/stylesheets',
                      src: ['**/*.sass'],
                      dest: './dist/public/stylesheets/',
                      ext: '.css'
                  }
              ]
          }
        },
        watch: {
            options: {
                livereload: true,
                spawn: true
            },
            ts_public: {
                files: ['src/public/**/*.ts'],
                tasks: ['ts:public']
            },
            ts_server: {
                files: ['src/server/**/*.ts'],
                tasks: ['ts:server']
            },
            views: {
                files: ['views/**/*.pug'],
                tasks: ['copy']
            },
            sass: {
                files: ['public/**/*.sass'],
                tasks: ['sass']
            },
            assets: {
                files: ['public/assets/**'],
                tasks: ['copy']
            }
        }
    });

    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-ts');
    grunt.loadNpmTasks('grunt-contrib-connect');
    grunt.loadNpmTasks('grunt-sass');

    grunt.registerTask('compileConfig', function () {
        let configSchema = JSON.parse(fs.readFileSync(path.join(__dirname, "schemas/all.json")).toString());
        let done = this.async(); //Wait for async execution
        schemaCompiler.compile(configSchema, 'ConfigSchema', {cwd: path.join(__dirname, "schemas")}).then(ts => {
            let dir = path.join(__dirname, 'src/common');
            let filepath = path.join(__dirname, 'src/common/definitions.d.ts');
            if (!fs.existsSync(dir)) {
                fs.mkdirSync(dir);
            }
            fs.writeFileSync(filepath, ts);
            done(true);
        });
    });

    grunt.registerTask('default', ['copy', 'compileConfig', 'ts', 'sass']);
};