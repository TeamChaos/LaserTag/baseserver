FROM node:10

WORKDIR /usr/baseserver

COPY package*.json ./
RUN npm install

COPY . .
RUN npm run grunt

EXPOSE 1984

CMD ["node", "bin/www"]
