/**
 * A simple tool to show notifications.
 * Usage: `new TextAlert("...", Level.info);`
 */

let nextID = 0;

export enum Level {
    NONE = "none",
    SUCCESS = "success",
    INFO = "info",
    WARNING = "warning",
    ERROR = "error",
}

export class TextAlert {

    constructor(private text: string, private level: Level) {
        let id = nextID;
        const dom_element = `
            <div class="container-alert text-alert alert-${level}" id="notification-${id}">
                ${text}
            </div>
        `
        addNotification(dom_element);
        setTimeout(function () {
            removeNotification(id)
        }, 8000);
    }
}


function initContainer() {
    let container = `
        <div id="notifications-container"></div>
    `
    let body = document.getElementsByTagName("body")[0];
    body.insertAdjacentHTML("afterend", container);
}

function addNotification(html_string: string) {
    let container = document.getElementById("notifications-container");
    container.insertAdjacentHTML("beforeend", html_string);
    nextID++;
}

function removeNotification(id: number) {
    let n = document.getElementById(`notification-${id}`);
    n.remove();
}

initContainer();