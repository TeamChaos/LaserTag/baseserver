/**
 * Window after ingame, showing some statistics.
 *
 * Content
 * - Overlay: Num players to be evaluated
 * - Winner
 * - All players. Sort after a category.
 * - if team based -> both team tables. Also, sortable.
 */

import {AppWSData, GameConfig, ScoreboardData, ScoreboardEntryData, TeamList} from "../common/definitions.js";
import {startWebSocket} from "./web_socket.js";
import {
    calculateTeamStatistics,
    ScoreboardEntries,
    scoreboardToObject,
    SortCriteria,
    sortEntities
} from "./statistics.js";
import {addRow, clearTable, getParentContainer, getTable, insertTableLayout} from "./player_tables.js";
import {Level, TextAlert} from "./notifications.js";
import {redirectOnStatus, showNotificationOnRedirect} from "./status_routes.js";
import {makeRequest} from "./http_requests.js";

let isEvaluation;
let config: GameConfig;
let isTeamBased: boolean;
let teamList: TeamList;
let scoreboardEntries: ScoreboardEntries;

const TABLE_ID_TEAMS = 100;
const TABLE_ID_PLAYERS = 101;

let twitterText: string;

let tweetsPlayer = [
    `TeamChaos just played a game Lasertag and %{winner} is an amazing human/genius.`,
    `%{winner} is a better Lasertag player than all his "friends".`,
    `%{winner} crushed the Teamchaos Lasertag exam and is from now on called "master %{winner}".`,
];

let tweetsTeam = [
    `TeamChaos just played a game Lasertag and team '%{winner}' absolutely destroyed team '%{loser}'.`,
]

/*
To be evaluated
 */

function updateToBeEvaluated(players: number[]) {
    let numPlayersLabel = document.getElementById("num-evaluations-label");
    let content: string = "";
    for (let id of players) {
        content += `${id}, `
    }
    content = content.slice(0, content.length - 2);
    numPlayersLabel.innerHTML = content;
}

/*
Statistics
 */

async function getStatistics() {
    const res = await makeRequest("GET", "/api/scoreboard", null);
    if (res.result === 0) {
        let scoreboardData: ScoreboardData = res.scoreboard;
        isTeamBased = scoreboardData.team;
        scoreboardEntries = scoreboardToObject(scoreboardData);
        initTables();
        updateWinner();
        updateStatisticsPlayers();
        if (isTeamBased) {
            updateStatisticsTeams();
        }
    }
}

function initTables() {
    function col(name: string, tableID: number, criteria: SortCriteria = null) {
        if (criteria) {
            let fun = tableID == TABLE_ID_TEAMS ? "updateStatisticsTeams" : "updateStatisticsPlayers";
            return `
                <span id="criteria-col-label-${criteria}-${tableID}" 
                class="criteria-col-label criteria-col-label-${tableID}" 
                onclick="${fun}('${criteria}')">${name}</span>`
        } else {
            return name;
        }
    }

    // Players
    let rows = [
        col("Position", TABLE_ID_PLAYERS),
        col("ID", TABLE_ID_PLAYERS, SortCriteria.id),
        col("Team", TABLE_ID_PLAYERS),
        col("Name", TABLE_ID_PLAYERS, SortCriteria.name),
        col("Kills", TABLE_ID_PLAYERS, SortCriteria.kills),
        col("Assists", TABLE_ID_PLAYERS, SortCriteria.assists),
        col("Deaths", TABLE_ID_PLAYERS, SortCriteria.deaths),
        col("Score", TABLE_ID_PLAYERS, SortCriteria.score)
    ];
    insertTableLayout(TABLE_ID_PLAYERS, "All Players", rows, getParentContainer());

    // Teams
    if (isTeamBased) {
        let rows = [
            col("Position", TABLE_ID_TEAMS),
            col("ID", TABLE_ID_TEAMS, SortCriteria.id),
            col("Name", TABLE_ID_TEAMS, SortCriteria.name),
            col("Kills", TABLE_ID_TEAMS, SortCriteria.kills),
            col("Assists", TABLE_ID_TEAMS, SortCriteria.assists),
            col("Deaths", TABLE_ID_TEAMS, SortCriteria.deaths),
            col("Score", TABLE_ID_TEAMS, SortCriteria.score)
        ];
        insertTableLayout(TABLE_ID_TEAMS, "Teams", rows, getParentContainer());
    }
}

function updateWinner() {
    let playerPositions = sortEntities(scoreboardEntries, SortCriteria.score);
    let teamStatistics: ScoreboardEntries;
    let teamsPositions: number[];
    if (isTeamBased) {
        teamStatistics = calculateTeamStatistics(scoreboardEntries, teamList);
        teamsPositions = sortEntities(teamStatistics, SortCriteria.score);
    }
    let team_player;
    let winner;
    if (isTeamBased) {
        winner = teamStatistics[teamsPositions[0]];
        team_player = "Team";
    } else {
        winner = scoreboardEntries[playerPositions[0]];
        team_player = "Player"
    }
    let winnerText = `${team_player} <b class="team-${winner.team}">${winner.name}</b> won this Game with <b>${winner.score}</b> points.`;
    let container = document.getElementById("winner-container");
    container.innerHTML = winnerText;

    // Twitter text
    let winnerName: string;
    let loserName: string;
    let randomItem = (array: any[]) => {
        return array[Math.floor(Math.random() * array.length)];
    }
    if (isTeamBased) {
        twitterText = randomItem(tweetsTeam);
        winnerName = teamStatistics[teamsPositions[0]].name;
        loserName = teamStatistics[teamsPositions[1]].name;
    } else {
        winnerName = randomItem([scoreboardEntries[playerPositions[0]].name, "Jörn"])
        loserName = scoreboardEntries[playerPositions[playerPositions.length - 1]].name;
        twitterText = randomItem(tweetsPlayer);
    }
    twitterText = twitterText.replace("%{winner}", winnerName).replace("%{loser}", loserName);
}

function updateStatisticsPlayers(sortCriteria: SortCriteria = SortCriteria.score) {
    console.assert(scoreboardEntries, "Scoreboard data must be set first");
    // Design
    let selectedCol = document.querySelector(`.criteria-col-label-${TABLE_ID_PLAYERS}[selected=true]`);
    if (selectedCol) {
        selectedCol.setAttribute("selected", "false");
    }
    document.getElementById(`criteria-col-label-${sortCriteria}-${TABLE_ID_PLAYERS}`).setAttribute("selected", "true");
    // Content
    let positions = sortEntities(scoreboardEntries, sortCriteria);
    let table = getTable(TABLE_ID_PLAYERS);
    clearTable(TABLE_ID_PLAYERS);
    let pos = 0;
    for (let id of positions) {
        let entry = scoreboardEntries[id];
        let data = [pos, entry.id, entry.team, entry.name,
            entry.kills, entry.assists, entry.deaths, entry.score].map((v => `${v}`));    // all to string
        let attributes = {class: `team-${entry.team}`};    // Add attribute for row here when needed
        addRow(table, attributes, data);
        pos++;
    }
}

function updateStatisticsTeams(sortCriteria: SortCriteria = SortCriteria.score) {
    console.assert(isTeamBased, "Cannot create team statistics, when is not team bases");
    console.assert(scoreboardEntries, "Scoreboard data must be set first");
    // Design
    let selectedCol = document.querySelector(`.criteria-col-label-${TABLE_ID_TEAMS}[selected=true]`);
    if (selectedCol) {
        selectedCol.setAttribute("selected", "false");
    }
    document.getElementById(`criteria-col-label-${sortCriteria}-${TABLE_ID_TEAMS}`).setAttribute("selected", "true");
    // Content
    let table = getTable(TABLE_ID_TEAMS);
    clearTable(TABLE_ID_TEAMS);
    let teamStatistics: ScoreboardEntries = calculateTeamStatistics(scoreboardEntries, teamList);
    let positions: number[] = sortEntities(teamStatistics, sortCriteria);
    for (let pos of positions) {
        let entry = teamStatistics[pos];
        let data = [pos, entry.id, entry.name,
            entry.kills, entry.assists, entry.deaths, entry.score].map((v => `${v}`));    // all to string
        addRow(table, {"class": `team-${entry.team}`}, data);
    }
}

/*
Control
 */

async function playAgain() {
    const res = await makeRequest("GET", "/api/reset", null);
    if (res.result == 1) {
        new TextAlert("Cannot start new Game: " + res.error, Level.WARNING);
    } else {
        window.location.replace("/lobby");
    }
}


/*
Communication
 */

async function getStatus() {
    const res = await makeRequest("GET", "/api/get_status", null);
    let state = res.status.state;
    redirectOnStatus(state, ["evaluation", "finished"]);
    let overlay = document.getElementById("overlay-container");
    if (state === "evaluation") {
        isEvaluation = true;
        updateToBeEvaluated(res.status.tobeevaluated);
        overlay.style.display = "fixed";
    } else {
        isEvaluation = false;
        await getStatistics();
        overlay.style.display = "None";
    }
}

async function getConfig() {
    const res = await makeRequest("GET", "/api/get_config", null);
    if (res.result === 0) {
        config = res.config;
        teamList = config.setup.teams;
    } else {
        new TextAlert("Cannot get config: " + res.error, Level.WARNING);
    }
}

function processMessage(msg: AppWSData) {
    console.log(msg)
    switch (msg.event) {
        case "players_tobeevaluated_changed": {
            updateToBeEvaluated(msg.list);
            break;
        }
        case "game_finished": {
            getStatus();
            break;
        }
    }
}

function onLoad() {
    showNotificationOnRedirect();
    getConfig().then(getStatus).catch(reason => console.error(reason));
    startWebSocket(processMessage);
}

window.onload = onLoad;
window["updateStatisticsPlayers"] = updateStatisticsPlayers;
window["updateStatisticsTeams"] = updateStatisticsTeams;
window["playAgain"] = playAgain;

function tweet() {
    let url = `https://twitter.com/intent/tweet?text=${twitterText}`;
    window.open(url, '_blank', 'location=yes,height=570,width=520,scrollbars=yes,status=yes');
}

window["tweet"] = tweet;