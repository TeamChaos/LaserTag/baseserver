/**
 * Functions to create and edit tables.
 */

import {TeamList} from "../common/definitions";

type Attributes = {
    [k: string]: string
}

const ID_CONTAINER_TABLES = "container-tables"; // Container id in .pug/HTML file
export const CLASS_TABLE = "table-players";
export const ID_PREFIX_TABLE = "table-players-";   // Add team id to get full dom id
export const TEAM_ID_NO_TEAMS = 0; // Team id when there are no teams. Table with all players has this id

export function addRow(table: HTMLTableElement, attributes: Attributes, cellsData: string[]) {
    let row = table.insertRow();
    for (const [key, value] of Object.entries(attributes)) {
        row.setAttribute(key, value);
    }

    for (let i = 0; i < cellsData.length; i++) {
        let data = cellsData[i];
        let cell = row.insertCell();
        cell.innerHTML = data;
    }
}

export function getTable(id: number) {
    return <HTMLTableElement>document.getElementById(`${ID_PREFIX_TABLE + id}`);
}

export function clearTables(className: string) {
    let tables = <HTMLCollectionOf<HTMLTableElement>>document.getElementsByClassName(className);
    for (let i = 0; i < tables.length; i++) {
        let table = tables[i];
        while (table.rows.length > 1) { // Don't delete table header columns
            table.deleteRow(table.rows.length - 1);
        }
    }
}

export function clearTable(id: number) {
    let table = getTable(id);
    while (table.rows.length > 1) { // Don't delete table header columns
        table.deleteRow(table.rows.length - 1);
    }
}

export function insertTableLayout(tableIdNumber: number, tableHeaderName: string, rows: string[], parentContainer: HTMLDivElement): HTMLTableElement {
    let thString = "";
    for (let th of rows) {
        thString += `<th>${th}</th>`
    }
    let table =
        `<div class="container-players">
            <div class="table-header">
                <span>${tableHeaderName}</span>
            </div>
            <table class=${CLASS_TABLE} id="${ID_PREFIX_TABLE + tableIdNumber}">
                <tr>${thString}</tr>
            </table>
        </div>   
        `;
    parentContainer.insertAdjacentHTML("beforeend", table);
    return <HTMLTableElement>document.getElementById(`${ID_PREFIX_TABLE + tableIdNumber}`);
}

export function buildTableLayouts(isTeamBased: boolean, container: HTMLDivElement, row_labels: string[], teamList: TeamList) {
    const NUM_TEAMS = 2;
    if (isTeamBased) {
        for (let i = 0; i < NUM_TEAMS; i++) {
            let table = insertTableLayout(i,
                `Team ${i}: <b class="team-name" id="team-name-${i}">${teamList[i].name}</b>`,
                row_labels,
                container);
            table.previousElementSibling.classList.add(`team-${i}`);    // table header
        }
    } else {
        insertTableLayout(TEAM_ID_NO_TEAMS, "Players", row_labels, container);
    }
}

export function getParentContainer() {
    return <HTMLDivElement>document.getElementById(ID_CONTAINER_TABLES);
}