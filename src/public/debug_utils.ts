/**
 * Add simple debug widgets to your site.
 * Don't use in production.
 */

const ID_CONTAINER_DEBUG = "container-debug";

let isEnabled = false;

function debugContainer() {
    return <HTMLDivElement>document.getElementById(ID_CONTAINER_DEBUG);
}

export function enableDebug() {
    isEnabled = true;
    let body = <HTMLBodyElement>document.getElementsByTagName("body")[0];
    let div = document.createElement("div");
    div.id = ID_CONTAINER_DEBUG;
    div.style.border = "2px solid red";
    div.style.background = "grey";
    body.prepend(div);
}

function addDebugElement(element: HTMLElement) {
    debugContainer().append(element);
}

export function addDebugButton(text: string, func: CallableFunction, ...args) {
    if (!isEnabled) return;
    let button = document.createElement("button");
    button.onclick = function () {
        func(args);
    };
    button.innerHTML = text;
    button.style.marginLeft = "10px";
    addDebugElement(button);
}

export function addDebugCheckbox(text: string, onSelect: CallableFunction, onDeselect: CallableFunction) {
    if (!isEnabled) return;
    let lbl = <HTMLLabelElement>document.createElement("label");
    lbl.innerHTML = text + ": ";
    lbl.style.color = "black";
    let cb = <HTMLInputElement>document.createElement("input");
    cb.type = "checkbox";
    cb.onchange = function () {
        if (cb.checked) {
            onSelect()
        } else {
            onDeselect();
        }
    };
    cb.style.opacity = "1";
    cb.style.position = "relative";
    cb.style.pointerEvents = "inherit";
    lbl.appendChild(cb);
    addDebugElement(lbl);
}
