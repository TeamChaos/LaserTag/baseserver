/**
 * Calculate statistics based on data from the server.
 */

import {ScoreboardData, ScoreboardEntryData, TeamList} from "../common/definitions";


export enum SortCriteria {
    name = "name", id = "id", score = "score", deaths = "deaths", kills = "kills", assists = "assists"
}

export interface ScoreboardEntries {
    [id: number]: ScoreboardEntryData
}

export function calculateTeamStatistics(scoreboardData: ScoreboardEntries, teamList: TeamList): ScoreboardEntries {
    let teamStatistics: ScoreboardEntries = {};
    for (let teamID in teamList) {
        if (teamList.hasOwnProperty(teamID)) {
            teamStatistics[teamList[teamID].id] = {
                id: teamList[teamID].id, name: teamList[teamID].name,
                score: 0, assists: 0, deaths: 0, kills: 0, team: teamList[teamID].id
            };
        }
    }
    for (let pId in scoreboardData) {
        if (scoreboardData.hasOwnProperty(pId)) {
            let entry = scoreboardData[pId];
            teamStatistics[entry.team].score += entry.score;
            teamStatistics[entry.team].assists += entry.assists;
            teamStatistics[entry.team].kills += entry.kills;
            teamStatistics[entry.team].deaths += entry.deaths;
        }
    }
    return teamStatistics;
}

export function sortEntities(entities: ScoreboardEntries, sortCriteria: SortCriteria, ascending: boolean = false): number[] {
    let positions: number[] = [];   // index is position, value is id
    for (let id in entities) {
        positions.push(+id);
    }
    positions.sort(function (id0, id1) {
        let ret: number;
        if (entities[id0][sortCriteria] > entities[id1][sortCriteria]) {
            ret = -1;
        } else if (entities[id0][sortCriteria] < entities[id1][sortCriteria]) {
            ret = 1;
        } else {
            ret = 0;
        }
        if (ascending) {
            return ret * -1;
        } else {
            return ret;
        }
    });
    return positions;
}

/**
 Convert from array to object to assure proper id access.
 */
export function scoreboardToObject(scoreboardData: ScoreboardData): ScoreboardEntries {
    let scoreboardEntries: ScoreboardEntries = {};
    for (let entry of scoreboardData.score) {
        scoreboardEntries[entry.id] = entry;
    }
    return scoreboardEntries;
}
