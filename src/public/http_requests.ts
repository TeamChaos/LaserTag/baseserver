/**
 * Wrapper for http requests to the server
 */
export function makeRequest(method: "POST" | "GET", url: string, params: object) {
    return new Promise<any>(((resolve, reject) => {
        let http = new XMLHttpRequest();
        http.open(method, url);
        http.onload = () => {
            if (http.status >= 200 && http.status < 300) {
                let res = JSON.parse(http.responseText);
                console.log({request: {url: url, data: params}, response: res});
                resolve(res);
            } else {
                reject({
                    status: http.status,
                    statusText: http.statusText
                });
            }
        }
        http.onerror = () => {
            reject({
                status: http.status,
                statusText: http.statusText
            });
        }
        let body = null;
        if (params != null) {
            body = JSON.stringify(params);
            http.setRequestHeader("Content-Type", "application/json");
        }
        http.send(body);
    }));
}