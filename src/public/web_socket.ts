/**
 * Wrapper for starting a websocket, with a connection to the server.
 */

export function startWebSocket(processMessage: CallableFunction) {
    console.log(location.protocol);
    const url = `${location.protocol === 'https:' ? 'wss' : 'ws'}://${location.host}/ws_app`;
    const ws = new WebSocket(url);
    ws.onopen = () => {
        console.info("Websocket opened");
    }
    ws.onclose = (e) => {
        console.info("Websocket closed. CloseEvent code: " + e.code);
    }
    ws.onerror = (e) => {
        console.error("Websocket error: " + e.type);
    }

    ws.onmessage = function (event) {
        try {
            let data = JSON.parse(event.data);
            processMessage(data);
        } catch (e) {
            if (e instanceof SyntaxError) {
                console.error("Invalid json received! msg: " + event.data + ", error: " + e);
            } else {
                console.error(e);
            }
        }
    }
}