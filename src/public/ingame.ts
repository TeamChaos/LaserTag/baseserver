/**
 *  Window for the running game.
 *  Content:
 *      - countdown
 *      - scoreboards
 *      - control buttons
 */

import {AppWSData, GameConfig, ScoreboardData, TeamList} from "../common/definitions";
import {Level, TextAlert} from "./notifications.js";
import {
    addRow,
    buildTableLayouts,
    CLASS_TABLE,
    clearTables, getParentContainer,
    getTable,
    insertTableLayout,
    TEAM_ID_NO_TEAMS
} from "./player_tables.js";
import {startWebSocket} from "./web_socket.js";
import {
    calculateTeamStatistics,
    ScoreboardEntries,
    scoreboardToObject,
    SortCriteria,
    sortEntities
} from "./statistics.js";
import {redirectOnStatus, showNotificationOnRedirect} from "./status_routes.js";
import {makeRequest} from "./http_requests.js";


let countdownInterval;
let stopTime;
let isRunning;  // false: Game is in starting state
let initializedTopScoreboards = false;
let teamList: TeamList;

/*
Timer
 */

function setupTimer(remaining: number) {
    if (countdownInterval) {
        clearInterval(countdownInterval);
    }
    let countdownRunning = document.getElementById("countdown-running");
    let countdownStarting = document.getElementById("countdown-starting");
    if (isRunning) {
        countdownRunning.classList.remove("transparent");
        countdownStarting.classList.add("transparent");
    } else {
        countdownRunning.classList.add("transparent");
        countdownStarting.classList.remove("transparent");
    }
    if (remaining > 0) {
        stopTime = Date.now() + remaining;
        countdownInterval = setInterval(function () {
            updateCountdownDisplay(stopTime - Date.now());
        }, 1000);
        updateCountdownDisplay(remaining);
    } else {
        updateCountdownDisplay(-1);
    }
}

function updateCountdownDisplay(time) {
    let countdown;
    if (isRunning) {
        countdown = document.getElementById("countdown-running");
    } else {
        countdown = document.getElementById("countdown-starting");
    }
    let minutes = Math.max(0, Math.floor(time / 1000 / 60));
    let seconds = Math.max(0, Math.floor((time / 1000) % 60));
    countdown.classList.remove("countdown-accent");
    if (minutes == 0) {
        if (seconds <= 10) {
            countdown.classList.add("countdown-accent");
            countdown.innerText = seconds;
        } else {
            countdown.innerText = seconds;
        }
    } else {
        countdown.innerText = minutes + ":" + ('0' + seconds).slice(-2);
    }
}

async function updateState() {
    let res = await makeRequest("GET", "/api/get_status", null);
    let state = res.status.state;
    redirectOnStatus(state, ["starting", "running"]);
    console.assert(state === "running" || state === "starting", `No countdown in '${state}' state`);
    let time = res.status.timer * 1000;
    let overlay = document.getElementById("overlay-container");
    if (state === "starting") { // Time till start
        isRunning = false;
        time = time * (-1);
        setupTimer(time);
        overlay.style.display = "fixed";
    } else {    // Time till finish
        isRunning = true;
        setupTimer(time);
        overlay.style.display = "None";
    }
}

/*
Top Scoreboard
 */

const TABLE_ID_TOP_PLAYERS = 100;
const TABLE_ID_TOP_TEAMS = 101;
const NUM_TOP_PLAYERS = 3;

function initTopScoreboards(isTeamBased: boolean) {
    let containerTop = <HTMLDivElement>document.getElementById("top-container");
    // Top players
    insertTableLayout(TABLE_ID_TOP_PLAYERS, "Top Players",
        ["Position", "ID", "Name", "Score"], containerTop);
    // Top Team
    if (isTeamBased) {
        insertTableLayout(TABLE_ID_TOP_TEAMS, "Top Teams",
            ["Position", "ID", "Name", "Score"], containerTop);
    }
    initializedTopScoreboards = true;
}

function updateTopPlayers(scoreboardData: ScoreboardEntries, positions: number[]) {
    let table = getTable(TABLE_ID_TOP_PLAYERS);
    for (let i = 0; i < NUM_TOP_PLAYERS; i++) {
        let player = scoreboardData[positions[i]];
        addRow(table, {"class": `team-${player.team}`},
            [`#${i + 1}`, `${player.id}`, player.name, `${player.score}`])
    }
}

function updateTopTeam(scoreboardData: ScoreboardEntries) {
    let teamStatistics = calculateTeamStatistics(scoreboardData, teamList);
    let positions = sortEntities(teamStatistics, SortCriteria.score);

    // Add data in html
    let table = getTable(TABLE_ID_TOP_TEAMS);
    for (let i = 0; i < positions.length; i++) {
        let team = teamStatistics[positions[i]];
        addRow(table, {"class": `team-${team.id}`}, [`#${positions[i]}`, `${team.id}`, team.name, `${team.score}`])
    }
}

/*
Teams/All Scoreboard
 */

function initScoreboard(isTeamBased: boolean) {
    buildTableLayouts(isTeamBased, getParentContainer(), ["id", "name", "kills", "assists", "deaths", "score"], teamList);
}

function updateScoreboard(scoreboardData: ScoreboardData) {
    let scoreboardEntries = scoreboardToObject(scoreboardData);
    let table: HTMLTableElement;
    let isTeamBased = scoreboardData.team;

    if (document.getElementsByClassName(CLASS_TABLE).length > 0) {
        clearTables(CLASS_TABLE);
    } else {
        initScoreboard(isTeamBased);
    }

    // Sort new data
    let positions = sortEntities(scoreboardEntries, SortCriteria.score);

    // Insert new table rows
    for (let pos of positions) {
        let entry = scoreboardEntries[pos];
        if (isTeamBased) {
            table = getTable(entry.team);
        } else {
            table = getTable(TEAM_ID_NO_TEAMS);
        }
        let data = [entry.id, entry.name, entry.kills, entry.assists, entry.deaths, entry.score].map((v => `${v}`));
        let attributes = {};    // Add attribute for row here when needed
        addRow(table, attributes, data);
    }

    // Update top scoreboards
    if (!initializedTopScoreboards) {
        initTopScoreboards(isTeamBased);
    }
    if (isTeamBased) {
        updateTopTeam(scoreboardEntries);
    }
    updateTopPlayers(scoreboardEntries, positions);
}

async function getScoreboard() {
    const res = await makeRequest("GET", "/api/scoreboard", null);
    if (res.result === 0) {
        updateScoreboard(res.scoreboard);
    }
}

/*
Control
 */

async function onBtnStopClick() {
    const res = await makeRequest("GET", "/api/stop", null);
    if (res.result !== 0) {
        new TextAlert(res.error, Level.ERROR);
    }
}

function onBtnPauseClick() {
    new TextAlert("Not implemented", Level.INFO);
}

/*
General
 */

async function getConfig() {
    const res = await makeRequest("GET", "/api/get_config", null);
    if (res.result === 0) {
        let conf: GameConfig = res.config;
        teamList = conf.setup.teams;
    }
}

function processMessage(msg: AppWSData) {
    console.log(msg);
    switch (msg.event) {
        case "scoreboard": {
            updateScoreboard(msg.scoreboard);
            break;
        }
        case "game_stopped": {
            window.location.replace("/evaluation");
            break;
        }
        case "game_started": {
            updateState();
            break;
        }
    }
}

function onLoad() {
    showNotificationOnRedirect();
    updateState()
        .then(getConfig)
        .then(getScoreboard)
        .catch((reason => {
            console.error(reason)
        }));
    startWebSocket(processMessage);
}

window.onload = onLoad;
window["onBtnStopClick"] = onBtnStopClick;
window["onBtnPauseClick"] = onBtnPauseClick;

// ======================================================================================
// ===============================     DEBUG     ========================================
// ======================================================================================
