/**
 * Website specific settings:
 * - background
 * - theme
 */

import {setEnabled} from "./background.js";

type themes = "dark" | "light" | "neon-blue" | "material" | "violet" | "reddish";

let settings = {
    theme: <themes>"dark",
    background: false,
};

function setTheme(themeName: themes) {
    document.getElementsByTagName("html")[0].setAttribute("theme", themeName);
    settings.theme = themeName;
    saveSettings();
}

function setBackgroundEnabled(enabled: boolean) {
    setEnabled(enabled);
    settings.background = enabled;
    saveSettings();
}

function saveSettings() {
    let cookieString = JSON.stringify(settings);
    cookieString = btoa(cookieString);
    document.cookie = `page_settings=${cookieString};SameSite=Strict`;
}

function loadSettings() {
    let cookieString = document.cookie;
    if (cookieString.length > 0) {
        let settingsString = cookieString.split(";")[0].slice("page_settings=".length);
        settingsString = atob(settingsString);
        settings = JSON.parse(settingsString);
        setTheme(settings.theme);
        setBackgroundEnabled(settings.background);
    }
}

loadSettings();

window["setTheme"] = setTheme;
window["setBackgroundEnabled"] = setBackgroundEnabled;
