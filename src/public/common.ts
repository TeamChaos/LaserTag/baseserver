/**
 * Functions that may be needed, in every page
 */
/*
 Materialize.js setup
*/


const isMobile = window.matchMedia("only screen and (max-width: 760px)").matches;
if (!isMobile) {
    // @ts-ignore
    M.AutoInit();
}

document.addEventListener('DOMContentLoaded', function () {
    initSelectors();
});

export function initSelectors() {
    let elements = document.querySelectorAll('select');

    if (!isMobile) {
        // @ts-ignore
        let instances = M.FormSelect.init(elements, undefined);
    } else {
        for (let i = 0; i < elements.length; i++) {
            elements[i].style.display = "initial";
        }
    }
}

function redirectToLogin() {
    window.location.replace("login");
}

window["redirectToLogin"] = redirectToLogin;