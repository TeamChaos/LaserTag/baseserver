/**
 * First Window of the game.
 * Content:
 *  - players and teams
 *  - settings for the game
 */

import {AppWSData, GameConfig, Player, PlayerList, TeamList} from "../common/definitions.js";
import {Level, TextAlert} from "./notifications.js";
import {
    addRow,
    buildTableLayouts,
    CLASS_TABLE,
    clearTables,
    getParentContainer,
    getTable,
    TEAM_ID_NO_TEAMS
} from "./player_tables.js";
import {startWebSocket} from "./web_socket.js";
import {addDebugButton, addDebugCheckbox} from "./debug_utils.js";
import {initSelectors} from "./common.js";
import {getGET, redirectOnStatus, showNotificationOnRedirect} from "./status_routes.js";
import {makeRequest} from "./http_requests.js";


let isTeamBased: boolean;
const ID_PREFIX_PLAYER_STATUS = "container-player-status-";
const CLASS_PLAYER_STATUS = "container-player-status";
const ID_BUTTON_NEXT_STATE = "btn-next-state";
const CLASS_ROW_PLAYER = "row-player";

let inReadyState = false;
let tobereadyBuffer;
let playerList: PlayerList;
let registeredPlayers: number[];
let teamList: TeamList;

const LENNARD_MODE = getGET("lennard") !== null;


/*
 Mobile view
*/

function onBtnOptionsClick() {
    switchView("players", "options");
}

function onBtnPlayersClick() {
    switchView("options", "players");
}

function switchView(from, to) {
    let btn_from = document.getElementById(`btn-${to}`);
    btn_from.style.display = "none";
    let btn_to = document.getElementById(`btn-${from}`);
    btn_to.style.display = "initial";
    let view_from = document.getElementById(`view-${from}`);
    view_from.style.display = "none";
    let view_to = document.getElementById(`view-${to}`);
    view_to.style.display = "inline-block";
}

/*
 Next state button
*/

function onBtnNextStateClick() {
    let btn = document.getElementById(ID_BUTTON_NEXT_STATE);
    let enabled = btn.classList.contains("enabled");
    if (!enabled) {
        if (inReadyState) {
            new TextAlert("Not all players are ready", Level.INFO);
        }
    } else {
        if (inReadyState) {
            start();
        } else {
            setReady();
        }
    }
}

/**
 * After all players are registered and configurations are finished.
 * From 'SETUP' to 'READY' State.
 */
async function setReady() {
    let btn = <HTMLButtonElement>document.getElementById(ID_BUTTON_NEXT_STATE);
    btn.disabled = true;
    let res = await makeRequest("GET", "/api/ready", null);
    if (res.result === 0) {
        setReadyDOM();
    } else {
        new TextAlert("Not ready yet:\n " + res.error, Level.INFO);
    }
    btn.disabled = false;
}

/**
 * After all players are ready.
 * From 'READY' to 'STARTING' State.
 */
async function start() {
    let res = await makeRequest("GET", "/api/start", null);
    if (res.result !== 0) { // ERROR
        new TextAlert("Failed to start game: " + res.error, Level.WARNING);
    } else {
        window.location.replace(`ingame`);
    }
}

/*
 Player tables
*/

function updateTables() {
    // Table headers
    if (isTeamBased) {
        for (let teamId in teamList) {
            if (teamList.hasOwnProperty(teamId)) {
                let nameElement = <HTMLInputElement>document.getElementById(`team-name-${teamId}`);
                nameElement.value = teamList[teamId].name;
            }
        }
    }

    // Players
    clearTables(CLASS_TABLE);
    if (registeredPlayers) {
        for (let player of registeredPlayers) {
            addPlayerToTeam(player, playerList[player].name, playerList[player].team);
        }
    }
}


/**
 * Change the content and appearance of the player tables.
 * When val is true, NUM_TEAMS tables are showed. Else only one table with all players.
 * @param val
 */
function setIsTeamBased(val: boolean) {
    let changed = val != isTeamBased;
    isTeamBased = val;
    if (changed) {
        let tablesContainer = getParentContainer();
        let playerRows = document.getElementsByClassName("row-player");
        let players: Player[] = [];
        for (let i = 0; i < playerRows.length; i++) {
            let row = <HTMLTableRowElement>playerRows[i];
            console.assert(row.hasAttribute("player-id"));
            console.assert(row.hasAttribute("player-team"));

            let playerID = Number(row.getAttribute("player-id"));
            let playerName = (<HTMLInputElement>document.getElementById("cellName-" + playerID)).value;
            let playerTeam = Number(row.getAttribute("player-team"));
            players.push({"name": playerName, "id": playerID, "team": playerTeam});
        }
        tablesContainer.innerHTML = "";
        buildTableLayouts(isTeamBased, tablesContainer, ["id", "name", "", "", "", ""], teamList);
        for (let player of players) {
            addPlayerToTeam(player.id, player.name, player.team);
        }
        if (isTeamBased) {
            teamNamesAsInput();
        }
    }
}

function teamNamesAsInput() {
    let teamNames = <HTMLCollectionOf<HTMLElement>>document.getElementsByClassName("team-name");
    for (let i = 0; i < teamNames.length; i++) {
        let element = teamNames[i];
        if (element.tagName !== "INPUT") {
            let newElement = document.createElement("input");
            let oldAttributes = teamNames[i].attributes;
            let newAttributes = newElement.attributes;
            for (let i = 0; i < oldAttributes.length; i++) {
                newAttributes.setNamedItem(<Attr>oldAttributes[i].cloneNode())
            }
            element.parentNode.replaceChild(newElement, element);
            newElement.value = element.innerHTML;

            newElement.addEventListener("change", () => {
                renameTeam(i, newElement.value);
            })
        }
    }
}

function updateReadyPlayers(tobereayPlayers: number[]) {
    let players = document.getElementsByClassName(CLASS_ROW_PLAYER);
    for (let i = 0; i < players.length; i++) {
        let p_id = Number(players[i].getAttribute("player-id"))
        let ready = !tobereayPlayers.includes(p_id);
        setPlayerReady(p_id, ready);
    }
    evaluatePlayersReadyStatus();
}


function setPlayerReady(id: number, ready: boolean) {
    let containerStatus = document.getElementById(ID_PREFIX_PLAYER_STATUS + id);
    if (ready) {
        containerStatus.innerHTML = `<span class="img-ready btnImg"/>`;
        containerStatus.setAttribute("status", "ready");
    } else {
        // containerStatus.innerHTML = `<span class="spinner-not-ready spinner-rotating"></span>`;
        containerStatus.innerHTML = `<span class="spinner-not-ready spinner-bouncy-balls">
                                        <span></span><span></span><span></span>
                                     </span>`;
        containerStatus.setAttribute("status", "not-ready");
    }
}


function evaluatePlayersReadyStatus() {
    let containers = document.getElementsByClassName(CLASS_PLAYER_STATUS);
    let allReady = true;
    for (let i = 0; i < containers.length; i++) {
        let statusContainer = containers[i];
        if (statusContainer.getAttribute("status") !== "ready") {
            allReady = false;
            break;
        }
    }
    let btnStart = document.getElementById(ID_BUTTON_NEXT_STATE);
    if (allReady) {
        btnStart.classList.remove("disabled");
        btnStart.classList.add("enabled");
    } else {
        btnStart.classList.remove("enabled");
        btnStart.classList.add("disabled");
    }
}

function finalizePlayers() {
    // remove 'remove' and 'move' button
    let removeElements = ["btn-remove", "btn-move"]
    for (let el of removeElements) {
        let removeElements = document.getElementsByClassName(el)
        for (let i = removeElements.length - 1; i >= 0; i--) {  // reverse because list is a live list
            removeElements[i].remove();
        }
    }
    // make name to label
    let nameInputs = <HTMLCollectionOf<HTMLInputElement>>document.getElementsByClassName("input-player-name");
    for (let i = nameInputs.length - 1; i >= 0; i--) {  // reverse because list is a live list
        nameInputs[i].readOnly = true;
    }
    // add status
    let playerRows = document.getElementsByClassName(`${CLASS_ROW_PLAYER}`);
    for (let i = playerRows.length - 1; i >= 0; i--) {  // reverse because list is a live list
        let playerID = playerRows[i].getAttribute("player-id");
        // insert in third (2) column
        playerRows[i].children[2].innerHTML = `<span class="${CLASS_PLAYER_STATUS}" id="${ID_PREFIX_PLAYER_STATUS + playerID}"></span>`
        setPlayerReady(Number(playerID), false);
    }
}

/**
 * Adds a new player to a team. Changes html-table.
 * Called from the server when a new player has connected.
 *
 * If gameMode is not team based: teamID can be the team in which the player was before changing the mode
 */
function addPlayerToTeam(playerID: number, name: string, teamID: number) {
    let table: HTMLTableElement;
    if (isTeamBased) {
        table = getTable(teamID);
    } else {
        table = getTable(TEAM_ID_NO_TEAMS);
    }
    console.assert(table != null, "Team table does not exist for team with id: " + teamID);
    let otherTeam = (teamID + 1) % 2;
    let data = [
        `<span id=cellId-${playerID}>${playerID}</span>`,
        `<input id=cellName-${playerID} class="input-player-name" type="text" value="${name}" onchange="renamePlayer(${playerID}, this.value)"/>`,
        `<button id=cellRemove-${playerID} class="btn-remove waves-effect waves-light btn red" onclick='removePlayer(${playerID})'>Remove</button>`,
    ];
    if (isTeamBased) {
        data.push(
            `<button id=cellMove-${playerID} class="btn-move waves-effect waves-light btn" onclick='movePlayerToTeam(${playerID}, ${otherTeam})'>Move to team ${otherTeam}</button>`);
    }
    let attributes = {
        "id": `${CLASS_ROW_PLAYER}-${playerID}`,
        "class": `${CLASS_ROW_PLAYER}`,
        "player-id": `${playerID}`,
        "player-team": `${teamID}`
    };
    addRow(table, attributes, data);
}

async function removePlayer(playerID: number) {
    let button = <HTMLButtonElement>document.getElementById("cellRemove-" + playerID);
    button.disabled = true;
    let row = document.getElementById(`${CLASS_ROW_PLAYER}-${playerID}`);
    let res = await makeRequest("GET", "/api/remove_player?player_id=" + playerID, null);
    if (res.result !== 0) { // ERROR
        new TextAlert("Failed to remove player: " + res.error, Level.INFO);
        button.disabled = false;
    } else {
        row.remove();
    }
}

async function movePlayerToTeam(playerID: number, teamID: number) {
    let button = <HTMLButtonElement>document.getElementById("cellMove-" + playerID);
    button.disabled = true;

    let res = await makeRequest("POST", `/api/config_player?player_id=${playerID}`, {
        "attribute": "team",
        "value": teamID
    });
    if (res.result !== 0) { // ERROR
        new TextAlert("Failed to update player value: " + res.error, Level.INFO);
        button.disabled = false;
    } else {
        let row = document.getElementById(`${CLASS_ROW_PLAYER}-${playerID}`);
        let playerName = (<HTMLInputElement>document.getElementById("cellName-" + playerID)).value;
        row.remove();
        addPlayerToTeam(playerID, playerName, teamID);
        playerList[playerID].team = teamID;
    }
}

async function renamePlayer(playerID: number, name: string) {
    let res = await makeRequest("POST", `/api/config_player?player_id=${playerID}`, {
        "attribute": "name",
        "value": name
    });
    if (res.result !== 0) { // ERROR
        new TextAlert("Failed to update player value: " + res.error, Level.INFO);
        (<HTMLInputElement>document.getElementById("cellName-" + playerID)).value = res.oldValue;
    } else {
        playerList[playerID].name = name;
    }
}

async function renameTeam(teamId: number, name: string) {
    let res = await makeRequest("POST", `/api/rename_team`, {
        "teamId": teamId,
        "name": name
    });
    if (res.result !== 0) { // ERROR
        new TextAlert("Failed to update team name " + res.error, Level.INFO);
        (<HTMLInputElement>document.getElementById("team-name-" + teamId)).value = res.oldValue;
    } else {
        teamList[teamId].name = name;
    }
}


/*
Config
 */

async function getConfig() {
    const res: any = await makeRequest("GET", "/api/get_config", null);
    if (res.result === 0) {
        updateConfig(res.config);
    }
}

function setDisabled(id: string, value: boolean) {
    let element = <HTMLInputElement>document.getElementById("config-" + id);
    element.disabled = value;
    if (value) {
        element.parentElement.classList.add("hide-option");
        element.parentElement.classList.remove("show-option");

    } else {
        element.parentElement.classList.add("show-option");
        element.parentElement.classList.remove("hide-option");
    }
}

/**
 * Wrapper for setting an option where the value was properly received from the server.
 * @param element
 * @param value
 * @param config
 */
function setOption(element: HTMLInputElement, value: any, config: string) {
    if (config === "gameMode") {
        setIsTeamBased(value !== "FreeForAll");
    } else if (config === "launchTrigger") {
        setDisabled("launchTime", {spawnpoint: true, time: false}[value]);
    }
    if (element.type === "checkbox") {
        element.checked = value;
    } else if (element.type === "number") {
        element.value = value;
    } else if (element.type === "select-one") {
        element.value = value;
        initSelectors();
    } else {
        console.error("Undefined element type: " + element.type);
    }
}


async function changeConfig(configid) {
    let element = <HTMLInputElement>document.getElementById("config-" + configid);
    let isBool = element.type && element.type === 'checkbox';
    let isNum = element.type && element.type === 'number';
    let isEnum = element.type && element.type === 'select-one';
    let newValue;
    if (isBool) {
        newValue = element.checked;
    } else if (isNum) {
        newValue = Number(element.value);
    } else if (isEnum) {
        newValue = element.value;
    }
    let schema = element.getAttribute("schema");
    let res = await makeRequest("POST", `/api/set_config/${schema}/${configid}`, {"value": newValue});
    if (res.result !== 0) { // ERROR
        new TextAlert("Failed to set value: " + res.error, Level.INFO);
        if (res.oldValue) {
            setOption(element, res.oldValue, configid);
        }
    }
}


function prepareConfig() {
    let opts = <HTMLCollectionOf<HTMLElement>>document.getElementsByClassName("config-opt");
    for (let i = 0; i < opts.length; i++) {
        let id = opts[i].id.replace("config-", "");
        opts[i].addEventListener("change", function () {
            changeConfig(id).then(r => {
            });
        });
    }
    if (LENNARD_MODE) {
        let optionsContainer = <HTMLCollectionOf<HTMLDivElement>>document.getElementsByClassName("option");
        for (let i = 0; i < optionsContainer.length; i++) {
            optionsContainer[i].style.marginLeft = `${Math.random() * 10}px`;
        }
    }

    teamNamesAsInput();
}

function disableOptions(disabled: boolean = true) {
    let optionsContainer = document.getElementById("view-options");
    let inputs = <NodeListOf<HTMLInputElement>>document.querySelectorAll("input, select");
    for (let i = 0; i < inputs.length; i++) {
        if (optionsContainer.contains(inputs[i])) {
            inputs[i].disabled = disabled;
        }
    }
}


/*
General
 */

function updateConfig(config: GameConfig) {
    playerList = config.setup.players;
    teamList = config.setup.teams;

    // Update Options
    let opts = <HTMLCollectionOf<HTMLInputElement>>document.getElementsByClassName("config-opt");
    for (let i = 0; i < opts.length; i++) {
        let element = opts[i];
        let id = opts[i].id.replace("config-", "");
        let schema = opts[i].getAttribute("schema");
        let value;
        if (schema === "setup") {
            value = config.setup[id];
        } else {
            value = config[id];
        }
        if (value === undefined) {
            console.warn("Undefined config option " + id);
        } else {
            setOption(element, value, id);
        }
    }

    updateTables();
}

function setReadyDOM() {
    let btn = document.getElementById(ID_BUTTON_NEXT_STATE);
    btn.classList.remove("enabled");
    btn.classList.add("disabled");
    btn.setAttribute("state", "ready");
    finalizePlayers();
    disableOptions(true);
    inReadyState = true;
}

async function getStatus() {
    const res = await makeRequest("GET", "/api/get_status", null);
    let state = res.status.state;
    redirectOnStatus(state, ["setup", "ready"]);
    let players = res.status.players;
    clearTables(CLASS_TABLE);
    for (let player of players) {
        addPlayerToTeam(player, playerList[player].name, playerList[player].team);
    }
    if (state === "ready") {
        setReadyDOM();
        let toBeReady = res.status.tobeready;
        updateReadyPlayers(toBeReady);
    }
}

function processMessage(message: AppWSData) {
    console.log(message);
    switch (message.event) {
        case "players_changed":
            registeredPlayers = message.list;
            updateTables();
            break;
        case "config_changed":
            updateConfig(message.config);
            break;
        case "game_ready":
            setReadyDOM();
            if (tobereadyBuffer) {
                updateReadyPlayers(tobereadyBuffer);
            }
            break;
        case "players_tobeready_changed":
            let players_tobeready = message.list;
            if (!inReadyState) {
                tobereadyBuffer = players_tobeready;
            } else {
                updateReadyPlayers(players_tobeready)
            }
            break;
        case "game_started":
            window.location.replace(`ingame`);
            break;
        case "scoreboard":
            break;  // TODO: ignore scoreboard event
        default:
            console.warn("Received an unknown event: " + message.event);
            break;
    }
}

function refresh() {
    getConfig().then(getStatus).then(prepareConfig).catch(reason => console.error(reason));
    let gameMode = <HTMLInputElement>document.getElementById("config-gameMode");
    let launchTrigger = <HTMLInputElement>document.getElementById("config-launchTrigger");
    launchTrigger.addEventListener("change", () => {
        setOption(launchTrigger, launchTrigger.value, "launchTrigger")
    });
}

function onLoad() {
    showNotificationOnRedirect();
    startWebSocket(processMessage);
    refresh();
}

window.onload = onLoad;

// Make functions visible to the window
window["onBtnNextStateClick"] = onBtnNextStateClick;
window["onBtnOptionsClick"] = onBtnOptionsClick;
window["onBtnPlayersClick"] = onBtnPlayersClick;
window["movePlayerToTeam"] = movePlayerToTeam;
window["removePlayer"] = removePlayer;
window["renamePlayer"] = renamePlayer;

// ======================================================================================
// ===============================     DEBUG     ========================================
// ======================================================================================

let debugPlayerID = 0;
let debugReadyIdx = 0;
let debugThemeIdx = 0;
let debugThemes = ["dark", "light", "neon-blue", "material", "violet", "redish"]

//enableDebug();
addDebugButton("Add player", debugAddPlayer);
addDebugButton("Set next player ready", debugSetPlayerReady);
addDebugButton("Set all players ready", debugSetAllPlayersReady);
addDebugCheckbox("Disable options", () => {
    disableOptions()
}, () => {
    disableOptions(false)
});
addDebugCheckbox("Dark theme",
    () => {
        document.getElementsByTagName("html")[0].setAttribute("theme", "dark")
    },
    () => {
        document.getElementsByTagName("html")[0].setAttribute("theme", "light")
    },
)
addDebugButton("Change theme", function () {
    let nextIdx = (debugThemeIdx++) % debugThemes.length
    document.getElementsByTagName("html")[0].setAttribute("theme", debugThemes[nextIdx]);
})
addDebugButton("Test Alert", function () {
    new TextAlert("Hello World", Level.NONE);
    new TextAlert("Hello World Lnger text sdada", Level.SUCCESS);
    new TextAlert("Hello World asdasd", Level.INFO);
    new TextAlert("Hello Worlda", Level.WARNING);
    new TextAlert("He", Level.ERROR);
});

function debugAddPlayer() {
    addPlayerToTeam(debugPlayerID++, randomName(Math.floor(Math.random() * 7 + 3)), 0);
}

function randomName(length) {
    let result = '';
    let characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    let charactersLength = characters.length;
    for (let i = 0; i < length; i++) {
        result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
}

function debugSetPlayerReady() {
    setPlayerReady(debugReadyIdx++, true);
}

function debugSetAllPlayersReady() {
    let playersRows = document.getElementsByClassName(CLASS_ROW_PLAYER);
    for (let i = 0; i < playersRows.length; i++) {
        let player = playersRows[i];
        let id = Number(player.getAttribute("player-id"));
        setPlayerReady(id, true);
    }
}
