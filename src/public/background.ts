/**
 * Animate the background.
 * When enabled, for example lines to the mouse.
 */

const canvas = <HTMLCanvasElement>document.getElementById("backgroundCanvas");
const ctx = canvas.getContext("2d");
let particles = [];
let mouse = {x: 0, y: 0};
let backgroundOn = false;
let idxParticle = 0;
const NUM_PARTICLES = 30;
const NEW_PARTICLES_PER_CLICK = 3;

function initBackground() {
    canvas.width = window.innerWidth;
    canvas.height = window.innerHeight;
    let distance = 100;
    // for (let x = 0; x < innerWidth / distance; x++) {
    //     particles.push(new LineBorderToMouse(x * distance, 0));
    //     particles.push(new LineBorderToMouse(x * distance, canvas.height));
    // }
    //
    // for (let y = 0; y < innerHeight / distance; y++) {
    //     particles.push(new LineBorderToMouse(0, y * distance));
    //     particles.push(new LineBorderToMouse(canvas.width, y * distance));
    // }
}

class LineBorderToMouse {
    private x;
    private y;
    private size = 5;
    private directionX;
    private directionY;
    private color;

    constructor(x, y) {
        this.x = x;
        this.y = y;
        this.directionX = Math.random() * 6 - 3;
        this.directionY = Math.random() * 6 - 3;
        this.color = Math.random() > 0.5 ? "#00FF55" : "#FF3333";
        this.color = `rgb(${Math.random() * 255}, ${Math.random() * 255}, ${Math.random() * 255})`;
    }

    draw() {
        ctx.beginPath();
        ctx.moveTo(this.x, this.y);
        ctx.lineTo(mouse.x, mouse.y);
        ctx.strokeStyle = this.color;
        ctx.stroke();
    }

    update() {
        if (this.x > canvas.width || this.x < 0) {
            this.directionX = -this.directionX;
        }
        if (this.y > canvas.height || this.y < 0) {
            this.directionY = -this.directionY;
        }

        this.x += this.directionX;
        this.y += this.directionY;
    }
}


class BeamMouseToBorder {
    private x;
    private y;
    private size = 3;
    private directionX;
    private directionY;
    private color;

    constructor(x, y) {
        this.x = x;
        this.y = y;
        this.directionX = Math.random() * 10 - 5;
        this.directionY = Math.random() * 10 - 5;
        this.color = Math.random() > 0.5 ? "#00FF55" : "#FF3333";
        //this.color = `rgb(${Math.random() * 255}, ${Math.random() * 255}, ${Math.random() * 255})`;
    }

    draw() {
        ctx.beginPath();
        ctx.moveTo(this.x, this.y);
        ctx.lineTo(this.x + this.directionX * 10, this.y + this.directionY * 10);
        ctx.strokeStyle = this.color;
        ctx.lineWidth = this.size;
        ctx.stroke();
    }

    update() {
        this.x += this.directionX;
        this.y += this.directionY;
    }
}

function animate() {
    requestAnimationFrame(animate);
    ctx.clearRect(0, 0, innerWidth, innerHeight);

    for (let i = 0; i < particles.length; i++) {
        particles[i].update();
        particles[i].draw();
    }
}

window.addEventListener("mousemove", e => {
    if (backgroundOn) {
        mouse.x = e.clientX;
        mouse.y = e.clientY;
        addClickParticles(mouse.x, mouse.y);
    }
});

window.addEventListener("click", e => {
    if (backgroundOn) {
        addClickParticles(e.clientX, e.clientY);
    }
});

function addClickParticles(x, y) {
    mouse.x = x;
    mouse.y = y;
    for (let i = 0; i < NEW_PARTICLES_PER_CLICK; i++) {
        let particle;
        particle = new BeamMouseToBorder(mouse.x, mouse.y)
        particle = new LineBorderToMouse(mouse.x, mouse.y)
        if (particles.length < NUM_PARTICLES) {
            particles.push(particle);
        } else {
            particles[idxParticle] = particle;
        }
        idxParticle++;
        idxParticle = idxParticle % NUM_PARTICLES;
    }
}


export function setEnabled(value: boolean) {
    backgroundOn = value;
    if (backgroundOn) {
        initBackground();
        animate();
    } else {
        particles = [];
    }
}
