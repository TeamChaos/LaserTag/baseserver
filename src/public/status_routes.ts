/**
 * Redirect the website to the correct url, based on the current status.
 */

import {Level, TextAlert} from "./notifications.js";

type States = "setup" | "ready" | "starting" | "running" | "evaluation" | "finished";

const GET_KEY = "from";

export function redirectOnStatus(status: States, allowed: States[]) {
    if (!allowed.includes(status)) {
        window.location.replace(
            {
                "setup": "/lobby", "ready": "/lobby",
                "starting": "/ingame", "running": "/ingame",
                "evaluation": "/evaluation", "finished": "/evaluation"
            }[status] + "?" + GET_KEY + "=" + window.location.pathname.slice(1));
        throw new Error("Redirect to other page, because status doesn't fit to current page.");
    }
}

export function getGET(key: string) {
    if (document.location.search.length > 0) {
        let query = document.location.search.replace("?", "").split("&");
        let vars = {};
        for (let i = 0; i < query.length; i++) {
            let split = decodeURIComponent(query[i]).split("=");
            vars[split[0]] = split[1];
        }
        if (vars.hasOwnProperty(key)) return vars[key];
    }
    return null;
}

export function showNotificationOnRedirect() {
    let redirect = getGET(GET_KEY);
    if (redirect != null) {
        new TextAlert(`Redirected from "${redirect}"`, Level.INFO);
    }
}
