import {makeRequest} from "./http_requests.js";
import {APIResponseData} from "../common/definitions.js";
import {Level, TextAlert} from "./notifications.js";


function onBtnGenerateClick() {
    let idField = document.getElementById('playerid');
    if (idField instanceof HTMLInputElement) {
        let playerid = Number(idField.value);
        if (isNaN(playerid)) {
            new TextAlert("Failed to generate QR code. Did not provide a valid number", Level.INFO);
        } else {
            retrieveQRCode(playerid).then(qrcode => {


                let canvas = document.getElementById("qr-canvas");
                if (canvas instanceof HTMLCanvasElement) {
                    let w = canvas.width;
                    let h = canvas.height;

                    let ctx = canvas.getContext('2d');
                    window["svg-temp"] = new Blob([qrcode], {type: "image/svg+xml;charset=utf-8"});
                    const domURL = self.URL || self.webkitURL || self;
                    const url = URL.createObjectURL(window["svg-temp"]);
                    let img = new Image();
                    img.onload = function () {
                        ctx.drawImage(img, 0, 0, w, h);
                        URL.revokeObjectURL(url);
                    };

                    img.src = url;

                    const centerX = (w - 150) / 2;
                    const centerY = (w - 90) / 2;

                    const logo = new Image();
                    logo.src = "./assets/qr_code_logo.png";
                    logo.onload = function () {
                        ctx.drawImage(logo, centerX, centerY, 150, 90);
                    }

                }
            })
                .catch(err => {
                    console.error("Failed to retrieve and display qr code: " + err);
                    new TextAlert(err, Level.INFO);
                });
        }


    }
}

async function retrieveQRCode(id: number) {
    const res: APIResponseData = await makeRequest("GET", "/api/generateAppConfig?id=" + String(id), null);
    if (res.result !== 0) { // ERROR
        throw Error("Failed to generate QR code on server " + res.error);

    } else {
        return res.qrcode;
    }
}


window["onBtnGenerateClick"] = onBtnGenerateClick;