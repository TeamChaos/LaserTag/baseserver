"""
Make API calls to debug/simulate functionalities.

Implemented as a rudimentary console.
"""
import time

import requests
import re
import websocket as ws
import threading
import json
import traceback

REGISTER_REGEX = re.compile(r"register(\s(?P<num>[0-9]*))")
READY_REGEX = re.compile(r"ready\s(?P<ids>(all)|(\d\s*)*\d)")
KILL_REGEX = re.compile(r"kill(\s(?P<killer>[0-9]*))(\s(?P<victim>[0-9]*))(\s(?P<assists>([0-9]\s?)*))")
SUBMIT_REGEX = re.compile(r"submit(\s(?P<num>[0-9]*))")
PAUSE_REGEX = re.compile(r"pause(\s(?P<num>[0-9]*(\.[0-9]*)?))")

default_programs = {
    "0": ["reset", "register 10"],
    "1": ["ready all"],
    "2": ["reset", "register 10", "api_ready", "ready all", "api_start", "connect"],
    "3": ["2", "pause 2", "api_stop"],
    "4": [f"submit {i}" for i in range(10)],
    "5": ["3", "4"]
}

help_msg = f"""
Dummy Clients
--------------
Actions:
  register (number)?
  ready (all) | (ids...)
  reset
  connect
  api_ready
  api_start
  help

Programs:
  {default_programs}"""


class Data:

    def __init__(self):
        self.running = True
        self.players = []
        self.ws = {}
        self.tobeready = []
        self._next_idx = 0

    def next_player_idx(self):
        self._next_idx += 1
        return self._next_idx - 1

    def add_player(self, p_id):
        self.players.append(p_id)
        self.tobeready.append(p_id)


data = Data()


def register(n: int):
    for i in range(n):
        p_id = data.next_player_idx()
        res = requests.get(f"http://localhost:1984/esp/register?player_id={p_id}")
        if res.status_code in (200, 204):
            data.add_player(p_id)
        else:
            print("Error: " + str(res))


def ready(ids: tuple):
    for p_id in ids:
        res = requests.get(f"http://localhost:1984/esp/set_ready?player_id={p_id}")
        if res.status_code in (200, 204):
            data.tobeready.remove(p_id)
            print(data.tobeready)
        else:
            print("Error: " + str(res))


def api_ready():
    res = requests.get(f"http://localhost:1984/api/ready")
    if res.status_code in (200, 204):
        print("set ready")
    else:
        print("Error: " + str(res))


def api_start():
    res = requests.get(f"http://localhost:1984/api/start")
    if res.status_code in (200, 204):
        print("set ready")
    else:
        print("Error: " + str(res))


def api_stop():
    res = requests.get(f"http://localhost:1984/api/stop")
    if res.status_code in (200, 204):
        print("Stop")
    else:
        print("Error: " + str(res))


def evaluation(pid: int):
    res_data = [
        [0, 1, 2, 3],
        [0, 1, 2, 3],
        [0, 1, 2, 3],
        [0, 1, 2, 3],
        [0, 1, 2, 3],
        [0, 1, 2, 3],
        [0, 1, 2, 3],
        [0, 1, 2, 3],
        [0, 1, 2, 3],
        [0, 1, 2, 3]
    ]
    res_data = json.dumps(res_data)
    res = requests.post(f"http://localhost:1984/esp/submit_result?player_id={pid}", res_data,
                        headers={"content-type": "application/json"})
    if res.status_code in (200, 204):
        print("submitted result")
    else:
        print("Error: " + str(res))


def send_kill(killer, victim, assists):
    obj = {"id": "kill",
           "victimID": victim,
           "offenderID": killer,
           "assists": assists}
    print(obj)
    data.ws[killer].send(json.dumps(obj))


def connect_web_sockets():
    def on_message(ws_, message, p):
        print(f"<- Player {p} message: {message}")

    def on_error(ws_, error, p):
        print(f"Player {p} error: {error}")

    def on_close(ws_, p):
        print(f"Player {p} closed")

    def on_open(ws_: ws.WebSocketApp, p):
        print(f"Player {p} opened")

    for player in data.players:
        def make_function(func, p):
            return lambda ws_, *args: func(ws_, *args, p)

        websocket = ws.WebSocketApp("ws://localhost:1984/ws_game",
                                    on_open=make_function(on_open, player)
                                    )
        # on_close=on_close, on_message=on_message, on_error=on_error, on_open=on_open)
        threading.Thread(target=websocket.run_forever).start()
        data.ws[player] = websocket


def parse_input(in_value: str):
    if in_value in default_programs.keys():
        for command in default_programs[in_value]:
            print(f">>>{command}")
            parse_input(command)
    elif re.fullmatch(REGISTER_REGEX, in_value):
        try:
            num = int(re.fullmatch(REGISTER_REGEX, in_value).group("num"))
        except (KeyError, ValueError):
            num = 1
        register(num)
    elif re.fullmatch(READY_REGEX, in_value):
        ids = re.fullmatch(READY_REGEX, in_value).group("ids")
        if ids == "all":
            ready(tuple(data.tobeready))
        else:
            ids = [int(i) for i in ids.split(" ")]
            ready(tuple(ids))
    elif in_value == "reset":
        data.__init__()
    elif in_value == "connect":
        connect_web_sockets()
    elif in_value == "api_ready":
        api_ready()
    elif in_value == "api_start":
        api_start()
    elif re.fullmatch(KILL_REGEX, in_value):
        match = re.fullmatch(KILL_REGEX, in_value)
        killer = int(match.group("killer"))
        victim = int(match.group("victim"))
        assists = [int(i) for i in match.group("assists").split(" ")]
        send_kill(killer, victim, assists)
    elif in_value == "api_stop":
        api_stop()
    elif re.fullmatch(SUBMIT_REGEX, in_value):
        match = re.fullmatch(SUBMIT_REGEX, in_value)
        pid = int(match.group("num"))
        evaluation(pid)
    elif re.fullmatch(PAUSE_REGEX, in_value):
        match = re.fullmatch(PAUSE_REGEX, in_value)
        t = float(match.group("num"))
        print(f"Sleep {t} seconds")
        time.sleep(t)
    elif in_value == "help":
        print(help_msg)
    else:
        print("ERROR: Invalid input")


def main():
    print(help_msg)
    while data.running:
        in_value = input(">")
        try:
            parse_input(in_value)
        except Exception as e:
            traceback.print_exc()


if __name__ == '__main__':
    main()
