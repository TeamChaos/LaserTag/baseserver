import {GameStatusData} from "../common/definitions";
import {Game} from "../server/game/game";

let mock = false;

export function enableGetStatusMock() {
    if (mock) {
        Game.prototype.getStatus = function (): GameStatusData {
            let data: GameStatusData = {players: [], state: undefined};
            data.players = [];
            data.timer = -25;
            data.state = "starting";
            return data;
        }
    }
}
