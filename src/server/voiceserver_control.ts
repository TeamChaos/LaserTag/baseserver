import {Ice} from "ice";
import {Murmur} from "../../lib/Murmur";
import {sprintf} from "sprintf-js";
import Communicator = Ice.Communicator;
import {Player, TeamList, VoiceServerInfo} from "../common/definitions";
import {GameServerEvents} from "./game/game";
import StrictEventEmitter from "strict-event-emitter-types";
import {EventEmitter} from "events";
import {Server} from "./server";


export class VoiceserverControl {
    private enabled: boolean = false;
    private teamChannelMap: Map<number, number> = new Map<number, number>();
    private rootChannel: number = 0;
    private readonly serverId: number = 1;
    private readonly teamRegex = /^Team(\d+)$/;
    private readonly teamFormat = "Team%d";
    private readonly playerFormat = "AppPlayer%d";
    private allowedObservers: Array<string> = new Array<string>();
    private allowedPlayers: Array<string> = new Array<string>();
    /**
     * Defines the channel a certain user should be in.
     * If not in this list, they should be in root channel
     */
    private assignedChannels: Map<string, number> = new Map<string, number>();
    /**
     * Defines the mute state a certain user should have
     * If not in this list, they should be unmuted
     */
    private assignedMuteStatus: Map<string, boolean> = new Map<string, boolean>();
    private ingameChatEnabled: boolean = false;
    /**
     * Only use to close it in a finally clause
     */
    private communicator: Communicator;

    constructor(private readonly host: string, private readonly port: number, private readonly secret: string, private readonly publicVoiceInfo: VoiceServerInfo) {
    }

    public registerObserver(name: string) {
        this.allowedObservers.push(name);
    }

    /**
     * Register listener for game events to update the voice server/client status accordingly
     */
    public prepare(eventEmitter: StrictEventEmitter<EventEmitter, GameServerEvents>) {
        this.enabled = true;
        eventEmitter.on('game_prepared', config => {
            if (config.setup.gameMode !== 'FreeForAll') {
                this.ingameChatEnabled = true;
                this.prepareChannels(config.setup.teams);

            } else {
                this.ingameChatEnabled = false;
            }
        });
        eventEmitter.on('game_starting', () => {
            this.allowedPlayers = new Array<string>();
            let players = new Array<Player>();
            let playerList = Server.instance.game.getConfig().setup.players;
            for (let playersKey in playerList) {
                let p: Player = playerList[playersKey];
                players.push(p);
                this.allowedPlayers.push(this.toClientName(p));
            }
            if (this.ingameChatEnabled) {
                this.movePlayersToTeams(players);
            } else {
                players.forEach(p => this.setMutePlayer(p, true));
            }
            this.enforce(true);
        });
        eventEmitter.on('game_stopped', () => {
            this.movePlayersToLobby();
            this.unmuteAll();
            this.enforce(false);
        });
        eventEmitter.on('kill', killEvent => {
            let victimName = sprintf(this.playerFormat, killEvent.victimID);
            if (Server.instance.game.getConfig().muteOnDeath) {
                this.setMutePlayer({id: killEvent.victimID, team: 0, name: ""}, true, true);
            }
        });
        eventEmitter.on('respawn', player => {
            this.setMutePlayer({id: player.id, team: 0, name: ""}, false, true);
        });
        eventEmitter.on('reset', () => {
            this.allowedPlayers = new Array<string>();
            this.movePlayersToLobby();
            this.unmuteAll();
            this.enforce(false);
        });
        eventEmitter.on('tick', (counter) => {
            //Occasionally enforce voice setup again in case new clients connected. We cannot (easily) listen to voice server events, so this is a simple workaround
            if (counter % 5 == 0) {
                this.enforce(false);
            }
        });
    }

    public movePlayersToLobby(enforce?: boolean) {
        this.assignedChannels.clear();
        if (enforce) this.enforce(false);
    }

    public movePlayersToTeams(players: Array<Player>, enforce?: boolean) {
        this.assignedChannels.clear();
        for (let i = 0; i < players.length; i++) {
            let p = players[i];
            let teamChannel = this.teamChannelMap.get(p.team);
            if (!teamChannel) {
                console.warn("Don't have a channel prepared for team " + p.team);
                teamChannel = this.rootChannel;
            }
            let expectedName = this.toClientName(p);
            this.assignedChannels.set(expectedName, teamChannel);
        }
        if (enforce) this.enforce(false);
    }

    public unmuteAll(enforce?: boolean) {
        this.assignedMuteStatus.clear();
        if (enforce) this.enforce(false);
    }

    public setMutePlayer(player: Player, muted: boolean, enforce?: boolean) {
        let name = this.toClientName(player);
        this.assignedMuteStatus.set(name, muted);
        if (enforce) this.enforce(false);
    }

    public enforce(kick: boolean) {
        console.log("Enforcing voiceserver status");
        this.assignedChannels.forEach((channel, name) => console.log(name + " should be in " + channel));
        this.assignedMuteStatus.forEach((muted, name) => console.log(name + " should be muted: " + muted));
        this.executeIce(async meta => {
            try {
                let server = await meta.getServer(this.serverId);
                let users = await server.getUsers();
                users.forEach(u => {
                    if (this.allowedObservers.indexOf(u.name) == -1) {
                        if (this.allowedPlayers.indexOf(u.name) == -1) {
                            if (kick) {
                                server.kickUser(u.session, "You are not part of the game or registered as an observer");
                                return;
                            }

                        }
                        let changed = false;
                        let targetChannel = this.assignedChannels.has(u.name) ? this.assignedChannels.get(u.name) : this.rootChannel;

                        if (u.channel != targetChannel) {
                            u.channel = targetChannel;
                            changed = true;
                        }
                        let t = this.assignedMuteStatus.has(u.name) && this.assignedMuteStatus.get(u.name);
                        if (u.mute != t) {
                            u.mute = t;
                            changed = true;
                        }

                        if (changed) server.setState(u);

                    }

                });
            } catch (e) {
                console.error("Failed to enforce player channels: " + e);
            }
        });
    }

    public getVoiceInfo() {
        return this.publicVoiceInfo;
    }

    private prepareChannels(teams: TeamList) {
        this.executeIce(async meta => {
            try {
                let server = await meta.getServer(this.serverId);

                //Delete unused channels
                let channels = await server.getChannels()
                let iterator = channels.values();
                let result = iterator.next();
                while (!result.done) {
                    let c = result.value;
                    if (c.parent === this.rootChannel) {
                        await server.removeChannel(c.id);
                    }
                    result = iterator.next();
                }

                // channels.forEach(c=>{
                //
                //     let matchedString = this.teamRegex.exec(c.name);
                //     if(matchedString){
                //         let teamID = Number(matchedString[1]);
                //         console.log("Found team "+teamID+" with channel id "+c.id);
                //         this.teamChannelMap.set(teamID,c.id);
                //     }
                // });

                //Create new channels
                for (let teamsKey in teams) {
                    let team = teams[teamsKey];
                    console.debug("Creating channel for team " + team.id);
                    this.teamChannelMap.set(team.id, await server.addChannel(sprintf(this.teamFormat, team.id), this.rootChannel));

                }
            } catch (e) {
                console.error("Failed to prepare channels: " + e);
            }
        });
    }

    private toClientName(player: Player): string {
        return sprintf(this.playerFormat, player.id);
    }

    /**
     * Prepare a communicator to the server
     * @private
     */
    private ice(): Promise<Murmur.MetaPrx | void> {
        let that: VoiceserverControl = this;
        return Ice.Promise.try(
            function () {
                let iceOptions = new Ice.InitializationData();
                iceOptions.properties = Ice.createProperties([], iceOptions.properties);
                // This seems required with newer versions of zeroc-ice
                iceOptions.properties.setProperty('Ice.Default.EncodingVersion', '1.0');
                iceOptions.properties.setProperty('Ice.ImplicitContext', 'Shared')
                that.communicator = Ice.initialize(iceOptions);
                that.communicator.getImplicitContext().put('secret', that.secret);
                var proxy = that.communicator.stringToProxy(sprintf('Meta:tcp -h %s -p %s', that.host, that.port));
                return Murmur.MetaPrx.checkedCast(proxy);
            }
        ).catch(error => console.log('[mubleapi2]: ' + error));
    }

    /**
     * Execute an ICE action on the server
     */
    private executeIce(action: (meta: Murmur.MetaPrx) => void) {
        let that: VoiceserverControl = this;
        this.ice().then(action).catch(
            function (error) {
                console.log('[mumbleapi]: ' + error);
            }
        );
    }
}