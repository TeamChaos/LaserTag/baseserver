import {Network} from "../network/network";
import {
    KillMessage,
    ReadyMessage, ForceRefreshMessage,
    registerMessages,
    StartMessage,
    StopMessage, ResetMessage, GamePointsMessage, RespawnMessage, DamageMessage
} from "../network/impl/messages";
import {Schema, Validator} from "jsonschema";
import {
    ESPStatus,
    GameConfig,
    GameSetup,
    GameStatusData, HitData,
    KillEventData,
    Player,
    PlayerList,
    ResultData,
    ScoreboardData,
    Team,
    TeamList
} from "../../common/definitions";
import {PlayerManager} from "./player_manager";
import StrictEventEmitter from "strict-event-emitter-types";
import {EventEmitter} from "events";
import {Message} from "../network/message";
import {Scoreboard} from "./scoreboard";
import {PlayerID} from "../general";
import {Server} from "../server";
import {DataNetwork} from "../network/impl/DataNetwork";

export interface GameServerEvents {
    config_changed: GameConfig;
    players_tobeready_changed: number[];
    players_changed: number[];
    players_tobeevaluated_changed: number[];
    game_started: void;
    game_prepared: GameConfig;
    game_allReady: void;
    game_starting: void;
    game_stopped: void;
    game_finished: ScoreboardData;
    scoreboard_updated: ScoreboardData;
    kill: KillEventData;
    hit: HitData;
    respawn: PlayerID;
    /**
     * Tick event once per second.
     * Argument: tickCounter
     */
    tick: number;
    reset: void;
    cpoint_captured: number;
}

export enum GameState {
    SETUP,
    READY,
    STARTING,
    RUNNING,
    EVALUATION,
    FINISHED
}

function stateIDToString(id: GameState): "setup" | "ready" | "starting" | "running" | "evaluation" | "finished" {
    switch (id) {
        case GameState.EVALUATION:
            return "evaluation";
        case GameState.RUNNING:
            return "running";
        case GameState.STARTING:
            return "starting";
        case GameState.READY:
            return "ready";
        case GameState.SETUP:
            return "setup";
        case GameState.FINISHED:
            return "finished";
        default :
            return "setup";
    }
}

function isTeamBased(mode: string) {
    return "FreeForAll" !== mode;
}

function hasOwnProperty(obj, prop) {
    var proto = obj.__proto__ || obj.constructor.prototype;
    return (prop in obj) &&
        (!(prop in proto) || proto[prop] !== obj[prop]);
}


class Flags {
    notifiedReady: boolean = false;
    startingTime: number = -1;
    startTime: number = -1;
    stopTime: number = -1;
}

export class Game {
    private readonly count: number = 10;
    private status: GameState;
    private tickCounter: number;
    private configValidator;
    private config: GameConfig;
    private playerManager: PlayerManager;
    private flags: Flags = new Flags();
    private scoreboard: Scoreboard;


    constructor(private readonly network: Network, private readonly eventEmitter: StrictEventEmitter<EventEmitter, GameServerEvents>, private readonly configSchema: Schema, defaultConfig: GameConfig) {
        this.status = GameState.SETUP;
        this.tickCounter = 0;
        registerMessages(this.network);
        this.network.setMessageListener(this.onMessage.bind(this));
        this.playerManager = new PlayerManager(this.count, this.eventEmitter);
        this.configValidator = new Validator();
        this.config = defaultConfig;
        this.validateConfig();
    }

    public debugInsertGameNetworkMessage(msg: string): void {
        if (this.network instanceof DataNetwork) {
            this.network.debugInsertMessage(msg);
        }
    }

    public stopAndReset(): void {
        this.eventEmitter.emit("reset");
        this.network.sendMessage(new ResetMessage());
        this.network.stop();
    }


    public isFinished(): boolean {
        return this.status == GameState.FINISHED;
    }


    /**
     * Finalize setup and notify players
     * @param callback
     */
    public prepareStart(callback: (err?: string) => any) {
        if (this.status != GameState.SETUP) {
            return callback("Not in setup phase");
        }
        if (this.playerManager.getPlayers().length == 0) {
            return callback("No players registered");
        }
        if (!this.validateConfig()) {
            return callback("Configuration is invalid");
        }
        if (isTeamBased(this.config.setup.gameMode) && this.countRegisteredTeams() < 2) {
            return callback("Less than two teams used in team based gamemode");
        }
        Server.instance.storeConfig(this.config);
        this.finalizePlayerlist();
        this.status = GameState.READY;
        this.playerManager.readyGame();
        this.eventEmitter.emit("game_prepared", this.config);
        this.network.sendMessage(new ReadyMessage());
        this.scoreboard = new Scoreboard(this.count, this.config.setup, this.eventEmitter);
        return callback();
    }

    public stop(callback?: (err?: string) => any) {
        if (this.status != GameState.RUNNING) {
            if (callback) {
                callback("Game not running");
            }
            return;
        }
        this.status = GameState.EVALUATION;
        this.playerManager.finishGame();
        this.eventEmitter.emit("game_stopped");
        this.network.sendMessage(new StopMessage());
        this.flags.stopTime = this.tickCounter;
        if (callback) {
            return callback();
        }
    }

    public start(callback: (err?: string) => any) {
        if (this.status != GameState.READY) {
            return callback("Not in ready state");
        }
        if (!this.playerManager.allReady()) {
            return callback("Players not ready");
        }
        this.status = GameState.STARTING;
        this.eventEmitter.emit("game_starting");
        this.network.sendMessage(new StartMessage());
        this.flags.startingTime = this.tickCounter;
        return callback();
    }

    public submitResults(id: PlayerID, results: ResultData): boolean {
        if (this.status != GameState.EVALUATION) {
            console.info("Game is not in evaluation state");
            return false;
        }
        if (!this.playerManager.isPlayerToByEvaluated(id.id)) {
            console.info("Player is already evaluated");
            return false;
        }
        this.scoreboard.processResult(id, results);
        if (results.hit_records) {
            console.debug("Hit records of player " + id.id + ": ")
            results.hit_records.forEach(n => console.log(n));
        }
        this.playerManager.evaluatePlayer(id.id);
        return true;
    }

    /**
     * Return the scoreboard data.
     * Returns null if the game has not been started yet
     */
    public getScoreboard(): ScoreboardData {
        if (this.status == GameState.STARTING ||
            this.status == GameState.RUNNING ||
            this.status == GameState.EVALUATION ||
            this.status == GameState.FINISHED) {
            return this.scoreboard.getScoreboardData();
        }
        return null;
    }

    /**
     * Call once per second
     */
    public tick() {
        this.tickCounter++;
        this.eventEmitter.emit("tick", this.tickCounter);
        this.network.tick();
        switch (this.status) {
            case GameState.READY:
                if (this.playerManager.allReady()) {
                    if (!this.flags.notifiedReady) {
                        this.eventEmitter.emit("game_allReady");
                        this.flags.notifiedReady = true;
                    }
                } else {
                    if (this.tickCounter % 5 == 0) {
                        this.network.sendMessage(new ReadyMessage());
                    }
                }
                break;
            case GameState.STARTING:
                if (this.config.setup.launchTrigger === "spawnpoint" || (this.config.setup.launchTrigger === "time" && this.tickCounter >= this.flags.startingTime + this.config.setup.launchTime)) {
                    console.log("Game started");
                    this.status = GameState.RUNNING;
                    this.flags.startTime = this.tickCounter;
                    this.eventEmitter.emit("game_started");
                }
                break;
            case GameState.RUNNING:
                let stop: boolean = false;
                if (this.config.stopTrigger === "time") {
                    if (this.tickCounter >= this.flags.startTime + this.config.timeLimit * 60) {
                        stop = true;
                    }
                } else if (this.config.stopTrigger === "score") {
                    if (this.scoreboard.getMaxScore() >= this.config.scoreLimit) {
                        stop = true;
                    }
                }
                if (stop) {
                    this.stop();
                }
                break;
            case GameState.EVALUATION:
                if (this.playerManager.isEvaluated()) {
                    this.status = GameState.FINISHED;
                    this.eventEmitter.emit("game_finished", this.scoreboard.getScoreboardData());
                } else if (this.tickCounter % 5 == 0) {
                    this.network.sendMessage(new StopMessage());
                }
                break;
        }

    }

    public registerPlayer(id: PlayerID): boolean {
        if (this.status == GameState.SETUP) {
            this.playerManager.registerPlayer(id.id);
            return true;
        } else if (this.status == GameState.READY) {
            return this.playerManager.isRegistered(id.id);
        }
        return false;
    }

    public readyPlayer(id: PlayerID): boolean {
        if (this.status == GameState.READY) {
            if (this.playerManager.readyPlayer(id.id)) {
                return true;
            } else {
                console.error("Trying to ready player %d, but they are not registered", id.id);
            }
        }
        return false;
    }

    /**
     * Return the GameSetup if the game is in ready state and the player is registered, otherwise this returns null.
     * @param id
     */
    public getSetupForPlayer(id: PlayerID): GameSetup {
        if (this.status == GameState.READY && this.playerManager.isRegistered(id.id)) {
            return this.config.setup;
        }
        return null;
    }

    public getStatusForPlayer(id: PlayerID, espStatus: Number): ESPStatus {
        let awaiting: number = 0;
        switch (this.status) {
            case GameState.SETUP:
                if (!this.playerManager.isRegistered(id.id)) {
                    awaiting = 1;
                }
                break;
            case GameState.READY:
                if (!this.playerManager.isReady(id.id)) {
                    awaiting = 2;
                }
                break;
            case GameState.EVALUATION:
                if (this.playerManager.isPlayerToByEvaluated(id.id)) {
                    awaiting = 3;
                }
                break;
        }
        let ok: boolean = espStatus == this.status && awaiting == 0;


        return {"ok": ok, "awaiting": awaiting, "status": this.status};
    }

    public sendForceUpdate() {
        this.network.sendMessage(new ForceRefreshMessage())
    }

    public removePlayer(id: PlayerID): boolean {
        return this.playerManager.removePlayer(id.id);
    }

    public getConfig(): GameConfig {
        return this.config;
    }

    /**
     * Gather and return status data
     */
    public getStatus(): GameStatusData {
        let data: GameStatusData = {players: [], state: undefined};
        data.state = stateIDToString(this.status);
        data.players = this.playerManager.getPlayers();
        if (this.status == GameState.READY) {
            data.tobeready = this.playerManager.getPlayersToBeReady();
        } else if (this.status == GameState.EVALUATION) {
            data.tobeevaluated = this.playerManager.getPlayersToBeEvaluated();
        } else if (this.status == GameState.STARTING) {
            data.timer = (this.tickCounter - this.flags.startingTime) - this.config.setup.launchTime;
        } else if (this.status == GameState.RUNNING) {
            if (this.config.stopTrigger === "time") {
                data.timer = (this.config.timeLimit * 60) - (this.tickCounter - this.flags.startTime);
            }
        }
        return data;
    }


    public configure(path: string[], param: string, value: any, callback: (err?: string, oldValue?: any) => any) {
        if (this.status != GameState.SETUP) return callback("Not in setup state");
        let old = JSON.stringify(this.config);
        let optionObj = this.config;
        for (let p of path) {
            if (!hasOwnProperty(optionObj, p)) return callback(`Option path ${path} does not exist`);
            optionObj = optionObj[p];
        }
        if (!hasOwnProperty(optionObj, param)) {
            return callback("Option " + param + " does not exist");
        }
        let oldValue;
        oldValue = optionObj[param];
        optionObj[param] = value;
        if (!this.validateConfig()) {
            this.config = JSON.parse(old);
            return callback("Invalid config", oldValue);
        }
        this.eventEmitter.emit("config_changed", this.config);
        return callback();
    }

    private countRegisteredTeams(): number {
        let players: PlayerList = this.config.setup.players;
        let registered: Array<number> = this.playerManager.getPlayers();
        let usedTeams: Array<number> = new Array<number>();
        for (let e in players) {
            let p: Player = players[e];
            let i: number = registered.indexOf(p.id);
            if (i != -1) {
                if (!usedTeams.includes(p.team)) {
                    usedTeams.push(p.team);
                }
            }
        }
        return usedTeams.length;
    }

    private finalizePlayerlist() {
        let finalPlayers: PlayerList = {};
        let finalTeams: TeamList = {};
        let players: PlayerList = this.config.setup.players;
        let teams: TeamList = this.config.setup.teams;
        let registered: Array<number> = this.playerManager.getPlayers();
        let usedTeams: Array<number> = new Array<number>();
        let teamBased: boolean = isTeamBased(this.config.setup.gameMode);
        for (let e in players) {
            let p: Player = players[e];
            let i: number = registered.indexOf(p.id);
            if (i != -1) {
                finalPlayers[e] = p;
                registered.splice(i, 1);
                if (teamBased) {
                    if (!usedTeams.includes(p.team)) {
                        usedTeams.push(p.team);
                    }
                } else {
                    finalPlayers[e].team = 0;
                }

            } else {
                console.warn("Player " + p.name + " is not registered");
            }
        }
        if (teamBased) {
            for (let e in teams) {
                let t: Team = teams[e];
                let i: number = usedTeams.indexOf(t.id);
                if (i != -1) {
                    finalTeams[e] = t;
                    usedTeams.splice(i, 1);
                } else {
                    console.warn("Configured Team " + t.name + " is not used");
                }
            }
            usedTeams.forEach(function (id, index, arrray) {
                console.warn("Team ID " + id + " is used but not configured");
                finalTeams["" + id] = {id: id, name: "Team " + id};
            });
        } else {
            finalTeams["0"] = {id: 0, name: "FreeForAll"};
        }


        registered.forEach(function (id, index, array) {
            console.warn("Player ID " + id + " is not configured");
            finalPlayers["" + id] = {id: id, name: "Player" + id, team: 0};
        });


        this.config.setup.players = finalPlayers;
        this.config.setup.teams = finalTeams;
    }

    private onMessage(msg: Message): boolean {
        console.log("Received message %s", msg.serializeJSON());
        if (msg instanceof KillMessage) {
            if (this.status === GameState.RUNNING) {
                this.scoreboard.processKill(msg);
                return true;
            } else {
                console.warn("KillMessage - Game not running but ", this.status);
            }

        } else if (msg instanceof GamePointsMessage) {
            if (this.status === GameState.RUNNING) {
                this.scoreboard.processGamePoint(msg);
                return true;
            } else {
                console.warn("GamePointMessage - Game not running but ", this.status);
            }
        } else if (msg instanceof RespawnMessage) {
            if (this.status === GameState.RUNNING) {
                this.eventEmitter.emit('respawn', msg.playerID);
            } else {
                console.warn("RespawnMessage - Game not running but ", this.status);
            }
        } else if (msg instanceof DamageMessage) {
            if (this.status === GameState.RUNNING) {
                let data: HitData = {
                    victimID: msg.offenderID.id,
                    amount: msg.amount,
                    type: msg.type
                }
                if (msg.offenderID) {
                    data.offenderID = msg.offenderID.id;
                }
                this.eventEmitter.emit('hit', data);
            } else {
                console.warn("DamageMessage - Game not running but ", this.status);
            }
        } else {
            console.warn("Received unknown message: " + msg);
        }
        return false;
    }


    private validateConfig(): boolean {
        let result = this.configValidator.validate(this.config, this.configSchema);
        if (!result.valid) {
            console.log("Validation errors: " + result.errors);
        }
        return result.valid;
    }
}