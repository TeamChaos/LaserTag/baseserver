import {GamePointsMessage, KillMessage} from "../network/impl/messages";
import StrictEventEmitter from "strict-event-emitter-types";
import {EventEmitter} from "events";
import {GameServerEvents} from "./game";
import {IntMap, PlayerID} from "../general";
import {
    GameSetup,
    KillEventData,
    Player,
    ResultData,
    ScoreboardData,
    ScoreboardEntryData
} from "../../common/definitions";

class RawScoreboardEntry {
    killed_by: Array<number>;
    deaths: number = 0;
    assisted_by: Array<number>;
    modePoints: number = 0;
    /**
     * Managed on server side. Should not be overridden, but only added to
     */
    serverModePoints: number = 0;

    constructor(count: number) {
        this.killed_by = new Array<number>(count);
        this.assisted_by = new Array<number>(count);
    }
}

class ScoreboardEntry implements ScoreboardEntryData {
    kills: number = 0;
    assists: number = 0;
    deaths: number = 0;
    score: number = 0;
    constructor(public name: string, public id: number, public team: number) {
    }

}


export class Scoreboard {

    private readonly scoreboard_raw: Array<RawScoreboardEntry>;
    private scoreboard_cached: ScoreboardData;
    private maxScore: number = 0;

    constructor(readonly count: number, private readonly config: GameSetup, private readonly eventEmitter: StrictEventEmitter<EventEmitter, GameServerEvents>) {
        this.scoreboard_raw = new Array<RawScoreboardEntry>(count);
        for (let i: number = 0; i < count; i++) {
            this.scoreboard_raw[i] = new RawScoreboardEntry(count);
        }
        this.scoreboard_cached = {
            score: new Array<ScoreboardEntry>(),
            team: config.gameMode !== "FreeForAll"
        };

        this.updateScoreboard();
    }

    public addServerGamePoints(playerID: PlayerID, points: number) {
        let playerBoard: RawScoreboardEntry = this.scoreboard_raw[playerID.id];
        playerBoard.serverModePoints += points;
        this.updateScoreboard();
    }

    public processGamePoint(msg: GamePointsMessage) {
        let playerBoard: RawScoreboardEntry = this.scoreboard_raw[msg.playerID.id];
        playerBoard.modePoints = msg.points;
        this.updateScoreboard();
    }

    public processKill(msg: KillMessage) {
        let victimBoard: RawScoreboardEntry = this.scoreboard_raw[msg.victimID.id];
        victimBoard.killed_by[msg.offenderID.id] = msg.kills;
        let newEvent: boolean = victimBoard.deaths != msg.deaths;
        victimBoard.deaths = msg.deaths;
        let assistingPlayers = [];
        for (let i: number = 0; i < this.count; i++) {
            if (victimBoard.assisted_by[i] != msg.assists[i]) {
                assistingPlayers.push(i);
            }
            victimBoard.assisted_by[i] = msg.assists[i];
        }
        this.updateScoreboard();
        if (newEvent) {
            let eventData: KillEventData;
            eventData = {
                victimID: msg.victimID.id,
                offenderID: msg.offenderID.id,
                assists: assistingPlayers
            };
            this.eventEmitter.emit("kill", eventData);
        }
    }

    public processResult(id: PlayerID, result: ResultData) {
        let playerBoard: RawScoreboardEntry = this.scoreboard_raw[id.id];
        playerBoard.deaths = result.scoreboard[id.id][2];//Deaths are in 3rd tuple
        playerBoard.modePoints = result.scoreboard[id.id][3]; //Mode points are at 4th place
        for (let i: number = 0; i < this.count; i++) {
            if (i !== id.id) {
                playerBoard.killed_by[i] = result.scoreboard[i][0]; //Kills by player i on this player
                playerBoard.assisted_by[i] = result.scoreboard[i][1]; //Assists
            }
        }
        this.updateScoreboard();
    }

    public getScoreboardData(): ScoreboardData {
        return this.scoreboard_cached;
    }

    public getMaxScore(): number {
        return this.maxScore;
    }

    private getPlayer(id: PlayerID): Player {
        for (let e in this.config.players) {
            // noinspection JSUnfilteredForInLoop
            let p: Player = this.config.players[e];
            if (id.id === p.id) {
                return p;
            }
        }
    }

    private updateMaxScore() {
        this.maxScore = 0;
        if (this.scoreboard_cached && this.scoreboard_cached.score) {
            let sum: IntMap<number> = {};
            let team: boolean = this.scoreboard_cached.team;
            for (let i in this.scoreboard_cached.score) {
                let val = this.scoreboard_cached.score[i].score;
                let id = team ? this.scoreboard_cached.score[i].team : this.scoreboard_cached.score[i].id;
                if (sum[id]) {
                    val += sum[id];
                }
                sum[id] = val;
            }
            for (let i in sum) {
                if (sum[i] > this.maxScore) {
                    this.maxScore = sum[i];
                }
            }
        }
    }

    private updateScoreboard() {
        let tmp: Array<ScoreboardEntry> = new Array<ScoreboardEntry>(this.count);
        for (let i: number = 0; i < this.count; i++) {
            tmp[i] = new ScoreboardEntry("ID" + i, i, -1);
        }

        for (let i: number = 0; i < this.count; i++) {
            let entry: RawScoreboardEntry = this.scoreboard_raw[i];
            console.log(entry);
            let player: Player = this.getPlayer(new PlayerID(i));
            if (player !== undefined) {
                tmp[i].name = player.name;
                tmp[i].id = player.id;
                tmp[i].team = player.team;
                tmp[i].deaths = entry.deaths;
                tmp[i].score += entry.modePoints;
                tmp[i].score += entry.serverModePoints;
                for (let j: number = 0; j < this.count; j++) {
                    let k: number = entry.killed_by[j];
                    if (k > 0) {
                        let other: Player = this.getPlayer(new PlayerID(j));
                        if (other) {
                            if (this.config.gameMode === "FreeForAll") {
                                tmp[j].kills += k;
                                tmp[j].score += this.config.killMultiplier * k;
                            } else {
                                let ff: boolean = player.team == other.team;
                                if (!this.config.friendlyFire && ff) {
                                    console.warn("There has been friendly fire even though it is disabled. " + i + " " + j);
                                }
                                if (ff) {
                                    tmp[j].score -= this.config.killMultiplier * k;
                                } else {
                                    tmp[j].kills += k;
                                    tmp[j].score += this.config.killMultiplier * k;
                                }
                            }
                        } else {
                            console.warn("Player " + i + " killed by non-existing player " + j);
                        }
                    }
                }
                for (let j: number = 0; j < this.count; j++) {
                    let k: number = entry.assisted_by[j];
                    if (k > 0) {
                        let other: Player = this.getPlayer(new PlayerID(j));
                        if (other) {
                            if (this.config.gameMode === "FreeForAll") {
                                tmp[j].assists += k;
                                tmp[j].score += this.config.assistMultiplier * k;
                            } else {
                                let ff: boolean = player.team == other.team;
                                if (!this.config.friendlyFire && ff) {
                                    console.warn("There has been friendly fire even though it is disabled. " + i + " " + j);
                                }
                                if (ff) {
                                    tmp[j].score -= this.config.assistMultiplier * k;
                                } else {
                                    tmp[j].assists += k;
                                    tmp[j].score += this.config.assistMultiplier * k;
                                }
                            }
                        } else {
                            console.warn("Player " + i + " assisted by non-existing player " + j);
                        }
                    }
                }
            }

        }
        this.scoreboard_cached.score = tmp.filter((value, index) => value.team >= 0);
        this.updateMaxScore();

        this.eventEmitter.emit("scoreboard_updated", this.scoreboard_cached);
    }
}