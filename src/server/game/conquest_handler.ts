import {CapturePointMessage, PointCapturedMessage} from "../network/impl/messages";
import {GameConfig, KillEventData} from "../../common/definitions";
import StrictEventEmitter from "strict-event-emitter-types";
import {EventEmitter} from "events";
import {GameServerEvents} from "./game";
import {Network} from "../network/network";
import {PlayerID} from "../general";
import {Scoreboard} from "./scoreboard";


class CapturePoint {
    private attackingTeam: number = -1;
    private lastAttack: number = 0;
    private attackCounter: number = 0;

    constructor(readonly flagID: number, private readonly captureCalllback: (point: CapturePoint) => void) {
        this.flagID = flagID;
        this.captureCalllback = captureCalllback;

    }

    private _controllingTeam: number = -1;

    get controllingTeam(): number {
        return this._controllingTeam;
    }

    private _attackingPlayers: Set<number> = new Set<number>();

    get attackingPlayers(): Set<number> {
        return this._attackingPlayers;
    }

    private _allInvolvedPlayers: Set<PlayerID> = new Set<PlayerID>();

    get allInvolvedPlayers(): Set<PlayerID> {
        return this._allInvolvedPlayers;
    }

    set allInvolvedPlayers(value: Set<PlayerID>) {
        this._allInvolvedPlayers = value;
    }

    public tick(): void {
        if (this.attackingTeam != -1) {
            this.attackCounter++;
            this.lastAttack++;
            if (this.lastAttack > 10) {
                this.cancelAttack();
            } else if (this.attackCounter > 20) {
                this.completeAttack();
            }
        }
    }

    public attack(player: PlayerID, team: number): void {
        if (team != this._controllingTeam) {
            this.attackingTeam = team;
            this.lastAttack = 0;
            this._attackingPlayers.add(player.id);
            this._allInvolvedPlayers.add(player);
        }
    }

    public onPlayerDeath(player: number): void {
        this.attackingPlayers.delete(player)
        //if(this.attackingPlayers.size==0) Maybe cancel capture
    }

    private completeAttack(): void {
        this._controllingTeam = this.attackingTeam;
        this.captureCalllback(this);
        this.cancelAttack();

    }

    private cancelAttack(): void {
        this.attackingTeam = -1;
        this.lastAttack = 0;
        this.attackCounter = 0;
        this._allInvolvedPlayers.clear();
        this._attackingPlayers.clear();
    }


}

export class ConquestHandler {

    private flagCount: number;
    private points: Array<CapturePoint>;
    private tickets: Map<number, number>;

    constructor(private readonly config: GameConfig, private readonly eventEmitter: StrictEventEmitter<EventEmitter, GameServerEvents>, private readonly network: Network, private readonly scoreboard: Scoreboard) {
        this.points = new Array<CapturePoint>(config.setup.capturePointCount);
        this.tickets = new Map<number, number>();
        for (let teamsKey in this.config.setup.teams) {
            this.tickets.set(Number(teamsKey), 100);
        }
        this.eventEmitter.on("kill", (kill: KillEventData) => {
            this.points.forEach((p: CapturePoint) => p.onPlayerDeath(kill.victimID));
            let team = this.config.setup.players[kill.victimID].team;
            this.tickets.set(team, Math.max(this.tickets.get(team) - 1, 0));
        });
        this.eventEmitter.on("tick", this.tick);
    }

    public onCaptureMessage(msg: CapturePointMessage, currentTickCounter: number): void {
        if (msg.pointID >= this.flagCount) {
            console.error("Received capture message for invalid point: " + msg.pointID);
            return;
        }
        let p: CapturePoint = (this.points)[msg.pointID];
        let team: number = this.config.setup.players[msg.playerID.id].team;
        p.attack(msg.playerID, team);
    }

    private onPointCaptured(point: CapturePoint): void {
        this.network.sendMessage(new PointCapturedMessage(point.flagID, point.controllingTeam, Array.from(point.attackingPlayers)));
        this.eventEmitter.emit("cpoint_captured", point.flagID);
        point.allInvolvedPlayers.forEach((p: PlayerID) => this.scoreboard.addServerGamePoints(p, this.config.setup.modePoints));
    }

    private tick(totalTicks: number): void {
        this.points.forEach(p => p.tick());
        if (totalTicks % 10 == 0) { //Process Tickets every 10 seconds
            this.tickets.forEach((teamID: number, tickets: number, map: Map<number, number>) => {
                let missingPoint = this.points.length - this.points.map(p => p.controllingTeam).filter(id => id == teamID).length;
                map.set(teamID, Math.max(0, tickets - missingPoint));
            })
        }
    }
}