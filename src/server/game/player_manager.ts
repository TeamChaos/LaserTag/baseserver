import StrictEventEmitter from "strict-event-emitter-types";
import {EventEmitter} from "events";
import {GameServerEvents} from "./game";

export class PlayerManager {

    private players: number[];
    private tobeready: number[];
    private tobeevaluated: number[];


    constructor(private readonly count, private readonly eventEmitter: StrictEventEmitter<EventEmitter, GameServerEvents>) {
        this.players = new Array<number>();
        this.tobeevaluated = new Array<number>();
        this.tobeready = new Array<number>();
    }

    /**
     * Register player
     * @return false if the player is already registered
     * @param id
     */
    public registerPlayer(id: number): boolean {
        if (this.players.includes(id)) {
            return false;
        }
        this.players.push(id);
        this.eventEmitter.emit("players_changed", this.players);
        return true;
    }

    /**
     * Marks a player ready
     * @return false if the player is not registered
     * @param id
     */
    public readyPlayer(id: number): boolean {
        if (this.tobeready.includes(id)) {
            this.tobeready.splice(this.tobeready.indexOf(id), 1);
            this.eventEmitter.emit("players_tobeready_changed", this.tobeready);
            return true;
        }
        return this.players.includes(id);
    }

    /**
     * Returns whether the player is registered
     * @param id
     */
    public isRegistered(id: number): boolean {
        return this.players.includes(id);
    }

    /**
     * Returns whether the player is registered and ready
     * @param id
     */
    public isReady(id: number): boolean {
        return this.players.includes(id) && !this.tobeready.includes(id);
    }

    /**
     * Marks a player evaluated
     * @return false if the player is not registered
     * @param id
     */
    public evaluatePlayer(id: number): boolean {
        if (this.tobeevaluated.includes(id)) {
            this.tobeevaluated.splice(this.tobeevaluated.indexOf(id), 1);
            this.eventEmitter.emit("players_tobeevaluated_changed", this.tobeevaluated);
            return true;
        }
        return this.players.includes(id);
    }

    public isPlayerToByEvaluated(id: number): boolean {
        return this.tobeevaluated.includes(id);
    }

    public readyGame() {
        this.tobeready = this.players.slice();
        this.eventEmitter.emit("players_tobeready_changed", this.tobeready);
    }

    /**
     * Return a copy of the registered player list
     */
    public getPlayers(): number[] {
        return this.players.slice();
    }

    public getPlayersToBeEvaluated(): number[] {
        return this.tobeevaluated.slice();
    }

    public getPlayersToBeReady(): number[] {
        return this.tobeready.slice();
    }

    /**
     * @return If there are players registered and if all of them are ready
     */
    public allReady(): boolean {
        return this.players.length > 0 && this.tobeready.length == 0;
    }

    public finishGame() {
        this.tobeevaluated = this.players.slice();
        this.eventEmitter.emit("players_tobeevaluated_changed", this.tobeevaluated);
    }

    public isEvaluated(): boolean {
        return this.tobeevaluated.length == 0;
    }

    /**
     Remove player from all lists,
     @return true if player existed
     */
    public removePlayer(id: number): boolean {
        if (this.players.includes(id)) {
            this.players.splice(this.players.indexOf(id), 1);
            this.eventEmitter.emit("players_changed", this.players);
            if (this.tobeevaluated.includes(id)) {
                this.tobeevaluated.splice(this.tobeevaluated.indexOf(id), 1);
                this.eventEmitter.emit("players_tobeevaluated_changed", this.tobeevaluated);
            }
            if (this.tobeready.includes(id)) {
                this.tobeready.splice(this.tobeready.indexOf(id), 1);
                this.eventEmitter.emit("players_tobeready_changed", this.tobeready);
            }

            return true;
        }
        return false;
    }
}