import StrictEventEmitter from "strict-event-emitter-types";
import {EventEmitter} from "events";
import {GameServerEvents} from "./game";
import {GameplayEventStamp, GameConfig, GameLog, GameplayEvent, KillEventData, HitData} from "../../common/definitions";
import {Schema, Validator} from "jsonschema";
import {PlayerID} from "../general";


export class GameRecorder {

    private readonly jsonValidator: Validator;
    private events: GameplayEventStamp[];
    private currentConfig: GameConfig;

    constructor(private readonly eventEmitter: StrictEventEmitter<EventEmitter, GameServerEvents>, private readonly eventSchema: Schema) {
        this.setupListeners(eventEmitter);
        this.jsonValidator = new Validator();
        this.events = new Array<GameplayEventStamp>();
    }

    public processAndRestGame(name: string): GameLog {
        let log = {name: name, events: this.events};
        this.events = new Array<GameplayEventStamp>();
        return log;
    }

    private setupListeners(eventEmitter: StrictEventEmitter<EventEmitter, GameServerEvents>) {
        eventEmitter.on('game_prepared', (config) => this.currentConfig = config);

        //Gameplay events
        eventEmitter.on('game_started', () => this.storeEvent({"event": "game_started"}));
        eventEmitter.on('game_starting', () => this.storeEvent({"event": "game_starting"}))
        eventEmitter.on('game_finished', (score) => this.storeEvent({"event": "game_finished", "scoreboard": score}));
        eventEmitter.on('game_stopped', () => this.storeEvent({"event": "game_stopped"}));
        eventEmitter.on('reset', () => this.storeEvent({"event": "reset"}));
        eventEmitter.on('kill', (data: KillEventData) => this.storeEvent({"event": "kill", "kill": data}));
        eventEmitter.on('hit', (data: HitData) => this.storeEvent({"event": "hit", "hit": data}));
        eventEmitter.on('respawn', (player: PlayerID) => this.storeEvent({
            "event": "respawn",
            "respawn_player": player.id
        }));
        eventEmitter.on('cpoint_captured', (point: number) => this.storeEvent({
            "event": "cpoint_captured",
            "cpoint": point
        }))

    }

    private storeEvent(event: GameplayEvent) {
        let result = this.jsonValidator.validate(event, this.eventSchema)
        if (!result.valid) {
            console.log("Cannot store invalid event: " + result.errors);
        } else {
            this.events.push({event: event, timestamp: +new Date()})
        }
    }
}