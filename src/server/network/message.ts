export interface RadioData {
    data: Array<number>,
    length: number
}

export interface JSONData {
    [k: string]: any
}

export class MessageID<T extends Message> {
    constructor(readonly id_str: string, readonly id_num: number) {
    }

    toJSON(): any {
        return this.id_str;
    }
}

export abstract class Message {


    constructor(readonly id: MessageID<Message>) {
    }


    abstract serializeRadio(): RadioData;

    abstract serializeJSON(): JSONData;
}