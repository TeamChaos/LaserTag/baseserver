import {JSONData, Message, MessageID, RadioData} from "./message";


export abstract class Network {


    /**
     * If the listener returns boolean the message should be broadcast (on client-server networks)
     */
    private listener: (msg: Message) => boolean;

    public abstract sendMessage(msg: Message);

    /**
     * Call once per tick
     */
    public tick() {

    }

    public stop() {

    }

    /**
     * Register a message type with the appropriate deserializers
     * @param id
     * @param deserializeRadio
     * @param deserializeJSON
     */
    public abstract registerMessage<T extends Message>(id: MessageID<T>, deserializeRadio: (d: RadioData) => T, deserializeJSON: (d: JSONData) => T): void;

    /**
     * Register a listener to any game messages.
     * There can only be one
     * @param callback
     */
    public setMessageListener(callback: (msg: Message) => boolean) {
        this.listener = callback;
    }

    /**
     * Handle a parsed message and pass it to the listener
     * If this returns true the message should be broadcast to other clients (if this a client-server network)
     * @param msg
     */
    protected handleMessage(msg: Message): boolean {
        if (this.listener) {
            return this.listener(msg);
        }
        return false;
    }
}