import {Network} from "../network";
import {JSONData, Message, MessageID} from "../message";
import * as WebSocket from "ws";
import {Server as WSServer} from "ws";
import {StringMap} from "../../general";
import {HelloMessage} from "./messages";


class ESPWebSocket extends WebSocket {
    lastAlive: number;
    /**
     * Might be unconfigured (-1)
     */
    playerID: number = -1;
}

export class DataNetwork extends Network {

    private deserializerMap: StringMap<(d: JSONData) => Message> = {};
    private debugTickTimer: number = 0;
    private interval_checkWebsocket: NodeJS.Timeout;

    constructor(private readonly wss: WSServer) {
        super();
        this.wss.on('connection', this.handleConnection.bind(this));
        if (this.interval_checkWebsocket) {
            console.error("Check interval still running");
            clearInterval(this.interval_checkWebsocket);
        }
        this.interval_checkWebsocket = setInterval(this.checkSockets.bind(this), 3000);
    }

    sendMessage(msg: Message) {
        console.log("Sending msg " + JSON.stringify(msg));
        let data: JSONData = msg.serializeJSON();
        let dataString: string = JSON.stringify(data);
        this.wss.clients.forEach(client => {
            if (client.readyState === WebSocket.OPEN) {
                client.send(dataString);
            }
        })
    }

    tick() {
        super.tick();
        if (process.env.DEBUG) {
            this.debugTickTimer++;
            if (this.debugTickTimer > 10) {
                console.log("Websockets stati:\n");
                this.wss.clients.forEach(client => {
                    console.log("Status: " + client.readyState);
                });
                this.debugTickTimer = 0;
            }
        }
    }


    stop() {
        this.wss.clients.forEach(client => client.close(1001, "Closing network"));
        this.wss.removeAllListeners();
        clearInterval(this.interval_checkWebsocket);
    }

    registerMessage<T extends Message>(id: MessageID<T>, deserializeRadio: any, deserializeJSON: (d: JSONData) => T): void {
        this.deserializerMap[id.id_str] = deserializeJSON;
    }

    public debugInsertMessage(msg: string): void {
        console.warn("Inserting debug websocket message into network stream");
        this.processMessage(null, msg);
    }

    private handleConnection(ws: ESPWebSocket) {
        console.log("New ESPWebsocket connection");
        ws.on('message', (data: string) => {
            if (this.processMessage(ws, data)) {
                this.wss.clients.forEach(client => {
                    if (client != ws) {
                        client.send(data);
                    }
                });
            }
        });
        ws.lastAlive = +new Date();
        ws.on('pong', () => ws.lastAlive = +new Date());
        ws.on('close', (code, reason) => console.log("ESPWebsocket closed %d %s", code, reason));
        ws.on('error', (err) => console.log("ESPWebsocket error %o", err));
        ws.on('unexpected-response', ((request, response) => console.log("Unexpected response %o %o", request, response)));
    }

    /**
     * Process an incoming string message
     * Returns true if the message should be broadcast
     * @param msgString
     * @param ws The websocket that received the message. Can be null
     */
    private processMessage(ws: ESPWebSocket | null, msgString: string) {
        try {
            let data: JSONData = JSON.parse(msgString);
            let deserializer: (d: JSONData) => Message = this.deserializerMap[data.id];
            if (!deserializer) {
                console.error("Unknown message type: %s (%s)", data.id, msgString);
                return false;
            }
            let msg: Message = deserializer(data);
            if (msg instanceof HelloMessage) {
                console.log("Device %d connected (%s)", msg.deviceID.id, msgString);
                if (ws) ws.playerID = msg.deviceID.id;
                return false;
            } else {
                return this.handleMessage(msg);
            }
        } catch (e) {
            console.error("Failed to process message %s: %o", msgString, e);
            return false;
        }

    }

    private checkSockets() {
        let timestamp: number = +new Date();
        // console.log("Checking websocket at %d", timestamp);
        this.wss.clients.forEach((ws: ESPWebSocket) => {
            if (timestamp - ws.lastAlive > 9000) {
                console.log("ESPWebsocket unresponsive. Terminating");
                return ws.terminate();
            }
            ws.ping();
        });
    }
}