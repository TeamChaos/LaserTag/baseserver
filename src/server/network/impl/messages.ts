import {JSONData, Message, MessageID, RadioData} from "../message";
import {PlayerID} from "../../general";
import {Network} from "../network";
import {ESPStatus} from "../../../common/definitions";

/**
 * Only used for websocket network
 */
const HelloMessageID = new MessageID<HelloMessage>("hello", -1);
const KillMessageID = new MessageID<KillMessage>("kill", 0);
const ReadyMessageID = new MessageID<SimpleMessage>("ready", 1);
const StartMessageID = new MessageID<SimpleMessage>("start", 2);
const StopMessageID = new MessageID<SimpleMessage>("stop", 3);
const ForceRefreshMessageID = new MessageID<SimpleMessage>("refresh", -1);
const ResetMessageID = new MessageID<SimpleMessage>("reset", 4);
const GamePointsMessageID = new MessageID<GamePointsMessage>("gamepoints", 5);
const RespawnMessageID = new MessageID<RespawnMessage>("respawn", 6);
const CapturePointMessageID = new MessageID<CapturePointMessage>("cpoint", 7);
const PointCapturedMessageID = new MessageID<PointCapturedMessage>("cpoint_captured", 8);
const DamageMessageID = new MessageID<DamageMessage>("dmg", -1);

class SimpleMessage extends Message {
    constructor(id: MessageID<any>) {
        super(id);
    }

    static getDeserializer(id: MessageID<any>): (d: JSONData | RadioData) => SimpleMessage {
        return (d: JSONData | RadioData) => new SimpleMessage(id);
    }

    serializeJSON(): JSONData {
        return Object.assign({}, this);
    }

    serializeRadio(): RadioData {
        return undefined;
    }
}

export class ReadyMessage extends SimpleMessage {
    constructor() {
        super(ReadyMessageID);
    }
}

export class StartMessage extends SimpleMessage {
    constructor() {
        super(StartMessageID);
    }
}

export class StopMessage extends SimpleMessage {
    constructor() {
        super(StopMessageID);
    }
}

export class ForceRefreshMessage extends SimpleMessage {
    constructor() {
        super(ForceRefreshMessageID);
    }
}

export class ResetMessage extends SimpleMessage {
    constructor() {
        super(ResetMessageID);
    }

}

export class RespawnMessage extends Message {
    constructor(readonly playerID: PlayerID) {
        super(RespawnMessageID);
    }

    static deserializeJSON(d: JSONData): RespawnMessage {
        let msg = Object.create(RespawnMessage.prototype);
        return Object.assign(msg, d, {
            id: RespawnMessageID,
            playerID: new PlayerID(d.playerID)
        });
    }

    static deserializeRadio(d: RadioData): RespawnMessage {
        if (d.length != 2) {
            throw new Error("Invalid packet size (" + d.length + ")");
        }
        let player = new PlayerID(d.data[1]);
        return new RespawnMessage(player);
    }

    serializeJSON(): JSONData {
        return Object.assign({}, this);
    }

    serializeRadio(): RadioData {
        let bytes: number[];
        bytes = new Array<number>(2);
        bytes[0] = this.id.id_num;
        bytes[1] = this.playerID.id;
        return {data: bytes, length: 2};
    }
}

export class KillMessage extends Message {

    constructor(readonly offenderID: PlayerID, readonly kills: number, readonly victimID: PlayerID, readonly deaths: number, readonly assists: number[]) {
        super(KillMessageID);
        if (assists.length != 10) {
            throw new Error("Must transmit 10 assists");
        }
    }

    static deserializeJSON(d: JSONData): KillMessage {
        let msg = Object.create(KillMessage.prototype);
        return Object.assign(msg, d, {
            id: KillMessageID,
            victimID: new PlayerID(d.victimID),
            offenderID: new PlayerID(d.offenderID)
        });
    }

    static deserializeRadio(d: RadioData): KillMessage {
        if (d.length != 14) {
            throw new Error("Invalid packet size (" + d.length + ")");
        }
        let off = new PlayerID(d.data[1] >> 4);
        let vic = new PlayerID(d.data[1] & 0b1111);
        return new KillMessage(off, d.data[2], vic, d.data[3], d.data.slice(4, 14));
    }

    serializeJSON(): JSONData {
        return Object.assign({}, this);
    }

    serializeRadio(): RadioData {
        let bytes: number[];
        let l: number = 14;
        bytes = new Array<number>(l);
        bytes[0] = this.id.id_num;
        bytes[1] = this.offenderID.id << 4 | this.victimID.id;
        bytes[2] = this.kills;
        bytes[3] = this.deaths;
        for (let i: number = 0; i < 10; i++) {
            bytes[i + 4] = this.assists[i];
        }
        return {data: bytes, length: l};
    }
}

export class GamePointsMessage extends Message {
    constructor(readonly playerID: PlayerID, readonly points: number) {
        super(GamePointsMessageID);
    }

    static deserializeJSON(d: JSONData): GamePointsMessage {
        let msg = Object.create(GamePointsMessage.prototype);
        return Object.assign(msg, d, {
            id: GamePointsMessageID,
            playerID: new PlayerID(d.playerID),
        });
    }


    serializeJSON(): JSONData {
        return Object.assign({}, this);
    }

    serializeRadio(): RadioData {
        return undefined;
    }
}

/**
 * Sent when the PMD receives a IR signal from a capture point beacon
 */
export class CapturePointMessage extends Message {
    constructor(readonly playerID: PlayerID, readonly pointID: number) {
        super(CapturePointMessageID);
    }

    static deserializeJSON(d: JSONData): CapturePointMessage {
        let msg = Object.create(CapturePointMessage.prototype);
        return Object.assign(msg, d, {
            id: CapturePointMessageID,
            playerID: new PlayerID(d.playerID),
        });
    }


    serializeJSON(): JSONData {
        return Object.assign({}, this);
    }

    serializeRadio(): RadioData {
        return undefined;
    }
}

/**
 * Sent when a capture point as been captured.
 * Includes the new controlling team as well as all players that participated
 */
export class PointCapturedMessage extends Message {
    constructor(readonly pointID: number, readonly teamID: number, readonly players: Array<number>) {
        super(CapturePointMessageID);
    }

    static deserializeJSON(d: JSONData): PointCapturedMessage {
        let msg = Object.create(PointCapturedMessage.prototype);
        return Object.assign(msg, d, {
            id: PointCapturedMessageID
        });
    }

    serializeJSON(): JSONData {
        return Object.assign({}, this);
    }

    serializeRadio(): RadioData {
        return undefined;
    }
}

/**
 * Only used for websockets. PMD should transmit its id with this
 */
export class HelloMessage extends Message {
    constructor(readonly deviceID: PlayerID) {
        super(HelloMessageID);
    }

    static deserializeJSON(d: JSONData): HelloMessage {
        let msg = Object.create(HelloMessage.prototype);
        return Object.assign(msg, d, {id: HelloMessageID, deviceID: new PlayerID(d.deviceID)});
    }

    serializeJSON(): JSONData {
        return Object.assign({}, this);
    }

    serializeRadio(): RadioData {
        return undefined;
    }
}

/**
 * Extended message only used with data network.
 * Allows detailed logging of damage taken.
 * offenderID can be -1 in json and null in JS
 *
 * DamageType:
 * 0 -> Shot
 * 1 -> Headshot
 * 2 -> Grenade
 * 3 -> Medkit
 */
export class DamageMessage extends Message {

    constructor(readonly playerID: PlayerID, readonly offenderID: PlayerID, readonly amount: number, readonly type: number) {
        super(DamageMessageID);
    }

    static deserializeJSON(d: JSONData): DamageMessage {
        let msg = Object.create(DamageMessage.prototype);
        return Object.assign(msg, d, {
            id: DamageMessageID,
            playerID: new PlayerID(d.playerID),
            offenderID: d.offenderID != -1 ? new PlayerID(d.offenderID) : null,
        });
    }

    serializeJSON(): JSONData {
        return Object.assign({}, this);
    }

    serializeRadio(): RadioData {
        return undefined;
    }

}


export function registerMessages(network: Network): void {
    network.registerMessage(KillMessageID, KillMessage.deserializeRadio, KillMessage.deserializeJSON);
    network.registerMessage(ReadyMessageID, SimpleMessage.getDeserializer(ReadyMessageID), SimpleMessage.getDeserializer(ReadyMessageID));
    network.registerMessage(StartMessageID, SimpleMessage.getDeserializer(StartMessageID), SimpleMessage.getDeserializer(StartMessageID));
    network.registerMessage(StopMessageID, SimpleMessage.getDeserializer(StopMessageID), SimpleMessage.getDeserializer(StopMessageID));
    network.registerMessage(HelloMessageID, undefined, HelloMessage.deserializeJSON);
    network.registerMessage(ForceRefreshMessageID, undefined, SimpleMessage.getDeserializer(ForceRefreshMessageID));
    network.registerMessage(ResetMessageID, SimpleMessage.getDeserializer(ResetMessageID), SimpleMessage.getDeserializer(ResetMessageID));
    network.registerMessage(GamePointsMessageID, undefined, GamePointsMessage.deserializeJSON);
    network.registerMessage(RespawnMessageID, RespawnMessage.deserializeRadio, RespawnMessage.deserializeJSON);
    network.registerMessage(CapturePointMessageID, undefined, CapturePointMessage.deserializeJSON);
    network.registerMessage(PointCapturedMessageID, undefined, PointCapturedMessage.deserializeJSON);
    network.registerMessage(DamageMessageID, undefined, DamageMessage.deserializeJSON);

}


