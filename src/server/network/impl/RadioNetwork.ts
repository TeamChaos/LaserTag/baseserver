import {Network} from "../network";
import {Message, MessageID, RadioData} from "../message";
import {IntMap} from "../../general";

export class RadioNetwork extends Network {

    private deserializerMap: IntMap<(d: RadioData) => Message> = {};

    sendMessage(msg: Message) {
        console.log("Sending msg " + msg);
    }

    registerMessage<T extends Message>(id: MessageID<T>, deserializeRadio: (d: RadioData) => T, deserializeJSON: any): void {
        this.deserializerMap[id.id_num] = deserializeRadio;
    }

}