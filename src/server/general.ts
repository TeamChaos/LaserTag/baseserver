export class PlayerID {

    public readonly id: number;

    constructor(id: any) {
        if (isNaN(id)) {
            throw new Error("ID " + id + " is not a number");
        }
        let num: Number = Number(id);
        if (num < 0 || num > 9) {
            throw new Error("ID " + id + " out of range");
        }
        this.id = num.valueOf();
    }

    toJSON(): any {
        return this.id;
    }
}

export interface StringMap<Q> {
    [key: string]: Q;
}

export interface IntMap<Q> {
    [key: number]: Q;
}