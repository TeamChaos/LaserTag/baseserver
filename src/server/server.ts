import * as bodyParser from "body-parser";
import * as cookieParser from "cookie-parser";
import * as express from "express";
import * as logger from "morgan";
import * as path from "path";
import {Server as WSServer} from "ws";
import * as basicAuth from "express-basic-auth";
import * as session from "express-session";
import * as uuid from "uuid";
import * as compare from "tsscmp";

//Import routes
import {IndexRoute} from "./routes";
import {PreGameLobbyRoute} from "./routes/pre_game_lobby";
import {Game, GameServerEvents} from "./game/game";
import {RadioNetwork} from "./network/impl/RadioNetwork";
import {Network} from "./network/network";
import {DataNetwork} from "./network/impl/DataNetwork";
import * as fs from 'fs';
import {Schema} from "jsonschema";
import * as $RefParser from "json-schema-ref-parser";
import {GameConfig, GameLog, VoiceServerInfo} from "../common/definitions";
import {APIRoute} from "./routes/api";
import {ESPRoute} from "./routes/esp";
import StrictEventEmitter from "strict-event-emitter-types";
import {EventEmitter} from "events";
import {prepareAppWebsockets} from "./app_websocket";
import errorHandler = require("errorhandler");
import {InGameRoute} from "./routes/ingame";
import {EvaluationRoute} from "./routes/post_game";
import {LoginRoute} from "./routes/login";
import {VoiceserverControl} from "./voiceserver_control";
import {AppConfigGeneratorRoute} from "./routes/app_config_generator";
import {GameRecorder} from "./game/game_recorder";

/**
 * The main server.
 * @class Server
 */
export class Server {
    private _voiceControl: VoiceserverControl;
    public static instance: Server;

    public app: express.Application;

    private gameEventEmitter: StrictEventEmitter<EventEmitter, GameServerEvents>;
    private gameWebSocket: WSServer;

    private gameRecorder: GameRecorder;

    get voiceControl(): VoiceserverControl {
        return this._voiceControl;
    }

    constructor(private readonly configSchema: Schema, private readonly defaultConfig: GameConfig, private readonly espAuthSecret: string, private readonly appAuthSecret: string, private readonly webPassword) {
        Server.instance = this;
        //create expressjs application
        this.app = express();

        //configure application
        this.config();

        //add routes
        this.routes();


        this.app.use(function (err, req, res, next) {
            console.error(err.stack);
            if (req.path.startsWith("/api/")) {
                res.status(200).send({result: 3});
            } else if (req.path.startsWith("/esp/")) {
                res.status(500);
            } else {
                next(err);
            }
        });
    }

    private _sessionParser = session({
        saveUninitialized: false,
        secret: '23v5_a!',
        resave: false
    });

    get sessionParser(): any {
        return this._sessionParser;
    }

    //TODO rework schema loading
    private static loadGameEventSchema(): Schema {
        //Ugly way of loading the schema synchronously. We need this before we start
        let data, done = false;
        $RefParser.dereference(path.join(__dirname, "../schemas/game_events.schema.json"), function (err, schema) {
            if (err) {
                console.error(err);
                return undefined;
            }
            data = schema;
            done = true;
        });
        require('deasync').loopWhile(function () {
            return !done
        });
        return data;
    }

    private _game: Game;

    get game(): Game {
        return this._game;
    }

    /**
     * Bootstrap the application.
     *
     * @class Server
     * @method bootstrap
     * @static
     * @return {ng.auto.IInjectorService} Returns the newly created injector for this app.
     */
    public static bootstrap(espAuthSecret: string, appAuthSecret: string, webPassword: string): Server {
        return new Server(this.loadConfigSchema(), this.loadDefaultConfig(), espAuthSecret, appAuthSecret, webPassword);
    }

    private static loadDefaultConfig(): GameConfig {
        let json: string;
        json = fs.readFileSync(path.join(__dirname, "../../config/default_config.json")).toString();
        return JSON.parse(json);
    }

    private static loadConfigSchema(): Schema {
        //Ugly way of loading the schema synchronously. We need this before we start
        let data, done = false;
        $RefParser.dereference(path.join(__dirname, "../schemas/game_config.schema.json"), function (err, schema) {
            if (err) {
                console.error(err);
                return undefined;
            }
            data = schema;
            done = true;
        });
        require('deasync').loopWhile(function () {
            return !done
        });
        return data;
    }

    public initialize(wsApp: WSServer, wsGame: WSServer) {
        this.gameEventEmitter = new EventEmitter();
        this.gameWebSocket = wsGame;
        if (process.env.VOICE_SERVER_HOST || process.env.VOICE_SERVER_PORT || process.env.VOICE_SERVER_PASSWORD || process.env.VOICE_SERVER_ICE_IP || process.env.VOICE_SERVER_ICE_PORT || process.env.VOICE_SERVER_ICE_SECRET) {
            if (process.env.VOICE_SERVER_HOST && process.env.VOICE_SERVER_PORT && process.env.VOICE_SERVER_PASSWORD && process.env.VOICE_SERVER_ICE_IP && process.env.VOICE_SERVER_ICE_PORT && process.env.VOICE_SERVER_ICE_SECRET) {
                console.log("Enabling voice server control");
                let info: VoiceServerInfo = {
                    available: true,
                    host: process.env.VOICE_SERVER_HOST,
                    port: Number(process.env.VOICE_SERVER_PORT),
                    password: process.env.VOICE_SERVER_PASSWORD
                };
                this._voiceControl = new VoiceserverControl(process.env.VOICE_SERVER_ICE_IP, Number(process.env.VOICE_SERVER_ICE_PORT), process.env.VOICE_SERVER_ICE_SECRET, info);

                this.voiceControl.registerObserver("GameObserver");
                this.voiceControl.movePlayersToLobby(true);
                this.voiceControl.prepare(this.gameEventEmitter);
            } else {
                console.warn("Found partial voice server configuration. You have to provide all environment variables to use this.")
            }
        }

        prepareAppWebsockets(wsApp, this.gameEventEmitter);

        this.gameRecorder = new GameRecorder(this.gameEventEmitter, Server.loadGameEventSchema())
        //Create a game instance
        this.createGame();

        setInterval(this.tick.bind(this), 1000);
    }

    /**
     * Configure application
     *
     * @class Server
     * @method config
     */
    public config() {
        this.app.disable('etag');
        this.app.use(express.static(path.join(__dirname, "../public")));

        //configure pug
        this.app.set("views", path.join(__dirname, "../views"));
        this.app.set("view engine", "pug");

        //use logger middlware
        this.app.use(logger("dev"));

        //use json form parser middlware
        this.app.use(bodyParser.json());

        //use query string parser middlware
        this.app.use(bodyParser.urlencoded({
            extended: true
        }));

        //use cookie parser middleware
        this.app.use(cookieParser());


        // //catch 404 and forward to error handler
        // this.app.use(function (err: any, req: express.Request, res: express.Response, next: express.NextFunction) {
        //     err.status = 404;
        //     next(err);
        // });
        //
        // //error handling
        // this.app.use(errorHandler());


    }

    /**
     * Create router
     *
     * @class Server
     * @method api
     */
    public routes() {
        let feRouter: express.Router = express.Router();
        let apiRouter: express.Router = express.Router();
        let espRouter: express.Router = express.Router();

        //Register routes TODO simplify the frontend routes. No idea why it is so complicated
        IndexRoute.create(feRouter);
        PreGameLobbyRoute.create(feRouter);
        InGameRoute.create(feRouter);
        EvaluationRoute.create(feRouter);
        LoginRoute.create(feRouter);
        AppConfigGeneratorRoute.create(feRouter);
        APIRoute.create(apiRouter);
        ESPRoute.create(espRouter);

        //Authentificators
        let espUsers = {};
        espUsers["tc"] = this.espAuthSecret;//TODO remove
        espUsers["esp"] = this.espAuthSecret;
        if (process.env.DEBUG) {
            espUsers["debug"] = "debug";
        }

        this.app.use(this.sessionParser, feRouter); //Use router middleware
        this.app.use('/api', this.sessionParser, apiRouter);
        this.app.use('/esp', basicAuth({users: espUsers}), espRouter);

        this.app.post('/login', function (req, res) {
            if (compare(req.body.password, Server.instance.webPassword)) {
                const id = uuid.v4();
                req.session.userId = id;
                res.redirect("/lobby");
            } else {
                res.sendStatus(401);
            }
        });

        this.app.get('/logout', function (request, response) {
            request.session.destroy(function () {
                response.send({result: 'OK', message: 'Session destroyed'});
            });
        });

    }

    public resetGame(callback?: (err?) => any) {
        //TODO maybe only allow if finished
        if (!this.game.isFinished()) {
            console.warn("Resetting unfinished game");
        }
        let d = new Date()
        let name = d.getFullYear() + "-" + ("0" + (d.getMonth() + 1)).slice(-2) + "-" + ("0" + d.getDate()).slice(-2) + "_" + ("0" + d.getHours()).slice(-2) + "-" + ("0" + d.getMinutes()).slice(-2) + "-" + ("0" + d.getSeconds()).slice(-2)
        let log = this.gameRecorder.processAndRestGame(name);
        this.storeGameLog(name, log);
        this.createGame();
        if (callback) callback();
    }

    public storeConfig(conf: GameConfig) {
        fs.writeFile(path.join(__dirname, "../../config/config.json"), JSON.stringify(conf), function (err) {
            if (err) {
                console.warn("Failed to store config to file: " + err);
            }
        });
    }

    private storeGameLog(name: string, log: GameLog) {
        fs.writeFile(path.join(__dirname, "../../game_logs/" + name + ".json"), JSON.stringify(log, null, 4 /*TODO remove space*/), function (err) {
            if (err) {
                console.warn("Failed to store game log to file: " + err);
            }
        });
    }

    private createGame() {
        if (this._game) {
            console.log("Stopping running game");
            this._game.stopAndReset();
        }
        let conf: GameConfig = this.defaultConfig;

        try {
            if (fs.existsSync(path.join(__dirname, "../../config/config.json"))) {
                conf = JSON.parse(fs.readFileSync(path.join(__dirname, "../../config/config.json")).toString());
            }
        } catch (e) {
            console.error("Failed to load existing config %e", e);
        }


        let network: Network;
        let networkType = process.env.NETWORK_TYPE;
        if (networkType === "radio") {
            network = new RadioNetwork();
        } else if (networkType === "data") {
            if (!this.gameWebSocket) {
                throw new Error("Game websocket server not initialized");
            }
            network = new DataNetwork(this.gameWebSocket);
        } else {
            throw new Error("Network type not specified or not known (" + networkType + ")");
        }
        console.log("Creating new game with network ", networkType);
        this._game = new Game(network, this.gameEventEmitter, this.configSchema, conf);
    }

    private tick() {
        if (this._game) {
            this._game.tick();
        }
    }
}