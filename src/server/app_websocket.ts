import StrictEventEmitter from "strict-event-emitter-types";
import {EventEmitter} from "events";
import {GameServerEvents} from "./game/game";
import * as WebSocket from "ws";
import {Server as WSServer} from "ws";
import {AppWSData, GameConfig, KillEventData} from "../common/definitions";


class AppWebSocket extends WebSocket {
    isAlive: boolean

}

function handleMessage(msg: string) {

}

function broadcast(msg: AppWSData, wss: WSServer) {
    //TODO validate messages against schema
    let msgString: string = JSON.stringify(msg);
    wss.clients.forEach(ws => {
        if (ws.readyState === WebSocket.OPEN) ws.send(msgString)
    });
}

export function prepareAppWebsockets(wss: WSServer, eventEmitter: StrictEventEmitter<EventEmitter, GameServerEvents>) {

    //Setup websocket server
    wss.on('connection', function (ws: AppWebSocket) {
        console.log("New AppWebsocket connection");
        ws.on('message', handleMessage);
        ws.on('error', function (err) {
            console.error("AppWebsocket Error %o", err);
        });
        ws.on('close', function (code, reason) {
            console.log("AppWebsocket closed %d %s", code, reason);
        });
        ws.isAlive = true;
        ws.on('pong', () => ws.isAlive = true);
    });

    setInterval(function () {
        wss.clients.forEach((ws: AppWebSocket) => {
            if (!ws.isAlive) {
                console.log("Unresponsive websocket. Terminating.")
                return ws.terminate();
            }

            ws.isAlive = false;
            ws.ping();
        });
    }, 10000);
    //TODO refactor websocket messages to use GameplayEvents (and GameserverEvents). Also merge event creation with game_recorder

    //Prepare messages
    eventEmitter.on('config_changed', (data: GameConfig) => broadcast({
        "event": "config_changed",
        "config": data
    }, wss));
    eventEmitter.on('players_changed', (data) => broadcast({"event": "players_changed", "list": data}, wss));
    eventEmitter.on('players_tobeevaluated_changed', (data) => broadcast({
        "event": "players_tobeevaluated_changed",
        "list": data
    }, wss));
    eventEmitter.on('players_tobeready_changed', (data) => broadcast({
        "event": "players_tobeready_changed",
        "list": data
    }, wss));

    eventEmitter.on('game_started', () => broadcast({"event": "game_started"}, wss));
    eventEmitter.on('game_starting', () => broadcast({"event": "game_starting"}, wss))
    eventEmitter.on('game_prepared', (data: GameConfig) => broadcast({"event": "game_ready", "config": data}, wss));
    eventEmitter.on('game_finished', (score) => broadcast({"event": "game_finished", "scoreboard": score}, wss));
    eventEmitter.on('game_stopped', () => broadcast({"event": "game_stopped"}, wss));
    eventEmitter.on('reset', () => broadcast({"event": "reset"}, wss));
    eventEmitter.on('scoreboard_updated', (score) => broadcast({"event": "scoreboard", "scoreboard": score}, wss));
    eventEmitter.on('kill', (data: KillEventData) => broadcast({"event": "kill", "kill": data}, wss));

}