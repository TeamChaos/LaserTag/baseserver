import {NextFunction, Request, Response, Router} from "express";
import {BaseRoute} from "./route";


/**
 * / route
 */
export class IndexRoute extends BaseRoute {

    /**
     * Create the routes.
     */
    public static create(router: Router) {

        //add home page route
        router.get("/", (req: Request, res: Response, next: NextFunction) => {
            new IndexRoute().index(req, res, next);
        });

        /**
         * Try to trick android clients into thinking it has internet access
         */
        router.get('/generate_204', function (req, res, next) {
            res.status(204).send("");
        });

        router.get('/gen_204', function (req, res, next) {
            res.status(204).send("");
        });

    }


    /**
     * The home page route.
     *
     * @class IndexRoute
     * @method index
     * @param req {Request} The express Request object.
     * @param res {Response} The express Response object.
     * @next {NextFunction} Execute the next method.
     */
    public index(req: Request, res: Response, next: NextFunction) {

        //set options
        let options: Object = {
            "message": "Welcome to Lasertag"
        };

        //render template
        this.render(req, res, "index", options);
    }
}