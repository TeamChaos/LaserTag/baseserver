import {NextFunction, Request, Response, Router} from "express";
import {BaseRoute} from "./route";
import {Server} from "../server";

const fs = require("fs");


/**
 * / route
 */
export class InGameRoute extends BaseRoute {

    /**
     * Create the routes.
     */
    public static create(router: Router) {

        //add home page route
        router.get("/ingame", (req: Request, res: Response, next: NextFunction) => {
            new InGameRoute().index(req, res, next);
        });

    }


    /**
     * The home page route.
     *
     * @class IndexRoute
     * @method index
     * @param req {Request} The express Request object.
     * @param res {Response} The express Response object.
     * @next {NextFunction} Execute the next method.
     */
    public index(req: Request, res: Response, next: NextFunction) {
        //render template
        this.render(req, res, "ingame");
    }


}
