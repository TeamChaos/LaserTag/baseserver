import {NextFunction, Request, response, Response, Router} from "express";
import {
    APIResponseData,
    AppConfig,
    GameConfig,
    GameStatusData,
    Player,
    ScoreboardData,
    TeamList
} from "../../common/definitions";
import {Server} from "../server";
import {PlayerID} from "../general";
import {VoiceserverControl} from "../voiceserver_control";
import * as  crypto from "crypto";
import * as QRCode from "qrcode";

export class APIRoute {


    /**
     * Create the routes.
     */
    public static create(router: Router) {

        //add home page route
        router.get("/", (req: Request, res: Response, next: NextFunction) => {
            res.json({result: 0})
        });


        router.get("/ready", this.enforceSession, this.setReady);
        router.get('/start', this.enforceSession, this.start);
        router.get("/stop", this.enforceSession, this.stop);
        router.get("/reset", this.enforceSession, this.reset);
        router.get("/scoreboard", this.getScoreboard);
        router.get("/get_status", this.getStatus);
        router.get("/get_config", this.getConfig);
        router.post("/set_config/:type/:param/", this.enforceSession, this.configure);
        router.get("/remove_player", this.enforceSession, this.removePlayer);
        router.post("/config_player", this.enforceSession, this.configPlayer)
        router.post("/rename_team", this.enforceSession, this.renameTeam)
        router.get("/send_force_refresh", this.enforceSession, this.sendForceRefresh);
        router.get("/voice_info", this.getVoiceServerInfo); //TODO require auth
        router.get('/generateAppConfig', this.enforceSession, this.generateQRCode);
        if (process.env.DEBUG) {
            router.get("/register_dummy_players", this.registerDummyPlayers);
            router.get("/ready_dummy_players", this.readyDummyPlayers);
            router.post("/insert_game_message", this.fakeGameWSMessage);
            router.get("/test_voice", this.testVoice);
            router.get("/move_voice_lobby", this.moveVoicePlayersToLobby);
        }


        //Keep at the end. Handle unknown actions
        router.get("/*", (req: Request, res: Response, next: NextFunction) => {
            res.json({result: 2, "error": "Unknown API call"})
        });
    }

    public static enforceSession(req: Request, res: Response, next: NextFunction) {
        if (req.session.userId || process.env.DEBUG) {
            next();
        } else {
            let response: APIResponseData = {result: 4, error: "You are not logged in"};
            console.warn("Unauthorized access");
            res.json(response);
        }
    }

    public static setReady(req: Request, res: Response) {
        Server.instance.game.prepareStart((err => {
            let response: APIResponseData = {result: 3};
            if (err) {
                response.result = 1;
                response.error = err;
            } else {
                response.result = 0;
            }
            res.json(response);
        }));
    }

    public static start(req: Request, res: Response) {
        Server.instance.game.start((err => {
            let response: APIResponseData = {result: 3};
            if (err) {
                response.result = 1;
                response.error = err;
            } else {
                response.result = 0;
            }
            res.json(response);
        }));
    }

    public static stop(req: Request, res: Response) {
        Server.instance.game.stop((err => {
            let response: APIResponseData = {result: 3};
            if (err) {
                response.result = 1;
                response.error = err;
            } else {
                response.result = 0;
            }
            res.json(response);
        }));
    }

    public static reset(req: Request, res: Response, next: NextFunction) {
        Server.instance.resetGame((err => {
            let response: APIResponseData = {result: 3};
            if (err) {
                response.result = 1;
                response.error = err;
            } else {
                response.result = 0;
            }
            res.json(response);
        }));
    }

    public static configure(req: Request, res: Response, next: NextFunction) {
        let param = req.params.param;
        let path: string[] = req.params.type === "setup" ? ["setup"] : [];
        let value = req.body.value;
        APIRoute.configAny(path, param, value, res);
    }

    public static removePlayer(req: Request, res: Response, next: NextFunction) {
        let id: PlayerID = APIRoute.parsePlayerID(req, res);
        let success = Server.instance.game.removePlayer(id);
        let response: APIResponseData = {result: 0, removed: success};
        res.json(response);
    }

    public static configPlayer(req: Request, res: Response, next: NextFunction) {
        let id: PlayerID = APIRoute.parsePlayerID(req, res);
        let attribute: string = req.body.attribute;
        let value: number | string = req.body.value;
        APIRoute.configAny(["setup", "players", `${id.id}`], attribute, value, res);
    }

    public static renameTeam(req: Request, res: Response, next: NextFunction) {
        let teamId = req.body.teamId;
        let name = req.body.name;
        let path = ["setup", "teams", teamId];
        APIRoute.configAny(path, "name", name, res);
    }

    public static configAny(path: string[], param: string, value: any, res: Response) {
        Server.instance.game.configure(path, param, value, ((err, oldValue) => {
            let response: APIResponseData = {result: 3};
            if (err) {
                response.result = 1;
                response.error = err;
                response.oldValue = oldValue;
            } else {
                response.result = 0;
            }
            res.json(response);
        }));
    }

    public static getScoreboard(req: Request, res: Response, next: NextFunction) {
        let board: ScoreboardData = Server.instance.game.getScoreboard();
        let response: APIResponseData;
        if (board) {
            response = {result: 0, scoreboard: board};
        } else {
            response = {result: 1};
        }
        res.json(response);
    }

    public static getConfig(req: Request, res: Response, next: NextFunction) {
        let conf: GameConfig = Server.instance.game.getConfig();
        let response: APIResponseData;
        if (conf == null) {
            response = {result: 1, error: "Not ready"};
        } else {
            response = {result: 0, config: conf};
        }
        res.json(response);
    }

    public static getStatus(req: Request, res: Response, next: NextFunction) {
        let status: GameStatusData = Server.instance.game.getStatus();
        let response: APIResponseData;
        response = {result: 0, status: status};
        res.json(response);
    }

    public static sendForceRefresh(req: Request, res: Response, next: NextFunction) {
        Server.instance.game.sendForceUpdate();
        res.json({result: 0});
    }

    public static getVoiceServerInfo(req: Request, res: Response) {
        let voiceControl: VoiceserverControl = Server.instance.voiceControl;
        let response: APIResponseData = {result: 0};
        if (voiceControl) {
            response.voiceserver = voiceControl.getVoiceInfo();
        } else {
            response.voiceserver = {available: false};
        }
        res.json(response);
    }

    public static generateQRCode(req: Request, res: Response) {
        let id: number = Number(req.query.id);
        let cname = isNaN(id) ? "app" : String(id);

        if (!process.env.APP_AUTH_SECRET_BASE || !process.env.EXTERNAL_HOST || !process.env.EXTERNAL_PORT || !process.env.EXTERNAL_IS_SECURE) {
            let response = {result: 3, message: "Server is missing required configuration"};
            return res.json(response);
        }
        //Generate secret
        //Same process as in /bin/www
        const base = process.env.APP_AUTH_SECRET_BASE;
        const secret = base + cname;
        let hasher = crypto.createHash('sha256');
            hasher.update(secret);
            const hash = hasher.digest('hex');
        const hashShort = hash.substr(0, 16);
        const credentials = cname + ":" + hashShort;

            //Generate base64 encoding of credentials
            const buff: Buffer = Buffer.from(credentials, 'utf-8');
            const credentialsBase64: string = buff.toString('base64');

            let data: AppConfig = {
                server: process.env.EXTERNAL_HOST,
                port: Number(process.env.EXTERNAL_PORT),
                secure: Boolean(process.env.EXTERNAL_IS_SECURE === "true"),
                playerid: id,
                credentials: credentialsBase64
            };
            let qrConfig = {
                errorCorrectionLevel: 'H',
                type: "svg",
                width: 1000,
                height: 1000
            };
            QRCode.toString(JSON.stringify(data), qrConfig).then(dataURL => {
                let response: APIResponseData = {result: 0, qrcode: dataURL};
                res.json(response);
            }).catch(err => {
                console.error("Failed to generate QR code " + err);
                let response: APIResponseData = {result: 3, message: "Failed to generate QR code"};
                res.json(response);
            });

    }


    /**
     * Try to parse the player id. If it is not valid, it will log it and send a 400 Bad Request response.
     * If this returns null, stop processing the request and don't send anything.
     * @param req
     * @param res
     */
    private static parsePlayerID(req: Request, res: Response): PlayerID {
        let id: PlayerID = null;
        try {
            id = new PlayerID(req.query.player_id);
        } catch (e) {
            console.error("No valid player id provided %o", e);
            let response: APIResponseData = {result: 2, "error": "No valid player id provided " + e};
            res.json(response);
        }
        return id;
    }

    private static registerDummyPlayers(req: Request, res: Response) {
        let success: boolean = Server.instance.game.registerPlayer(new PlayerID(0));
        success == success && Server.instance.game.registerPlayer(new PlayerID(1));
        success == success && Server.instance.game.registerPlayer(new PlayerID(2));
        success == success && Server.instance.game.registerPlayer(new PlayerID(3));

        if (success) {
            let response: APIResponseData = {result: 0};
            res.json(response);
        } else {
            let response: APIResponseData = {result: 1, "message": "Could not register players"};
            res.json(response);
        }
    }

    private static readyDummyPlayers(req: Request, res: Response) {
        let success: boolean = Server.instance.game.readyPlayer(new PlayerID(0));
        success == success && Server.instance.game.readyPlayer(new PlayerID(1));
        success == success && Server.instance.game.readyPlayer(new PlayerID(2));
        success == success && Server.instance.game.readyPlayer(new PlayerID(3));

        if (success) {
            let response: APIResponseData = {result: 0};
            res.json(response);
        } else {
            let response: APIResponseData = {result: 1, "message": "Could not ready players"};
            res.json(response);
        }
    }

    private static fakeGameWSMessage(req: Request, res: Response) {
        let msg = req.body;
        Server.instance.game.debugInsertGameNetworkMessage(JSON.stringify(msg));
        res.json({result: 0});
    }

    private static moveVoicePlayersToLobby(req: Request, res: Response) {
        Server.instance.voiceControl.movePlayersToLobby();
        Server.instance.voiceControl.unmuteAll(true);
        res.json({result: 0});
    }

    private static testVoice(req: Request, res: Response) {
        let players: Array<Player> = new Array<Player>();
        players.push({id: 1, name: "Max", team: 1});
        players.push({id: 0, name: "Kevin", team: 0});
        Server.instance.voiceControl.movePlayersToTeams(players);
        Server.instance.voiceControl.setMutePlayer({id: 1, name: "Max", team: 1}, true, true);
        res.json({result: 0});
    }
}

/**
 * Decorator to slow down the call of a function:
 * Only for testing.
 * Usage:
 *
 * @slowDown
 * function someFunction(...) {}
 */
function slowDown(target, key, descriptor) {
    const slowDownTimeMS = 3000;
    if (descriptor === undefined) {
        descriptor = Object.getOwnPropertyDescriptor(target, key);
    }
    let originalMethod = descriptor.value;
    descriptor.value = (...args) => {
        return setTimeout(() => originalMethod.apply(this, args), slowDownTimeMS);
    };
    return descriptor;
}