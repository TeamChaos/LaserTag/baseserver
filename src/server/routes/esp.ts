import {NextFunction, Request, Response, Router} from "express";
import {PlayerID} from "../general";
import {Server} from "../server";
import {ESPStatus, GameSetup, ResultData} from "../../common/definitions";
import {Schema, Validator, ValidatorResult} from "jsonschema";
import * as fs from "fs";
import * as basic_auth from "basic-auth";

export class ESPRoute {


    private static resultSchema: Schema;
    private static validator: Validator = new Validator();

    /**
     * Create the routes.
     */
    public static create(router: Router) {
        ESPRoute.loadSchemas();
        //add home page route
        router.get("/", (req: Request, res: Response, next: NextFunction) => {
            res.sendStatus(204);
        });

        router.get('/register', this.register);
        router.get('/set_ready', this.setReady);
        router.get('/get_setup', this.getSetup);
        router.post('/submit_result', this.submitResult);
        router.get('/get_status', this.getStatus)
    }

    public static register(req: Request, res: Response, next: NextFunction) {
        let id: PlayerID = ESPRoute.parsePlayerID(req, res);
        if (!id) return;
        if (Server.instance.game.registerPlayer(id)) {
            res.sendStatus(204);
        } else {
            res.sendStatus(503);
        }
    }

    public static setReady(req: Request, res: Response, next: NextFunction) {
        let id: PlayerID = ESPRoute.parsePlayerID(req, res);
        if (!id) return;
        if (Server.instance.game.readyPlayer(id)) {
            res.sendStatus(204);
        } else {
            res.sendStatus(503);
        }
    }

    public static getSetup(req: Request, res: Response, next: NextFunction) {
        let id: PlayerID = ESPRoute.parsePlayerID(req, res);
        if (!id) return;
        let setup: GameSetup = Server.instance.game.getSetupForPlayer(id);
        if (setup) {
            res.status(200).json(setup);
        } else {
            res.sendStatus(503);
        }
    }

    public static getStatus(req: Request, res: Response, next: NextFunction) {
        let id: PlayerID = ESPRoute.parsePlayerID(req, res);
        if (!id) return;
        let esp_status: Number = undefined;
        if (!isNaN(req.query.esp_status)) {
            esp_status = Number(req.query.esp_status);
        }
        let result: ESPStatus = Server.instance.game.getStatusForPlayer(id, esp_status);
        if (result) {
            if (result.ok) {
                res.sendStatus(204);
            } else {
                res.status(200).json(result);
            }
        } else {
            res.sendStatus(503);
        }
    }

    public static submitResult(req: Request, res: Response, next: NextFunction) {
        let id: PlayerID = ESPRoute.parsePlayerID(req, res);
        if (!id) return;
        let data: ResultData = req.body;
        let validation: ValidatorResult = ESPRoute.validator.validate(data, ESPRoute.resultSchema);
        if (validation.valid) {
            if (Server.instance.game.submitResults(id, data)) {
                res.sendStatus(204);
            } else {
                res.sendStatus(503);
            }
        } else {
            res.sendStatus(400);
            console.warn("Got invalid results from %d: %o %o", id.id, data, validation.errors);
        }

    }

    private static loadSchemas() {
        fs.readFile(__dirname + "/../../../schemas/results.schema.json", function (err, data) {
            if (err) {
                console.error("Failed to load result schema %o", err);
            } else {
                ESPRoute.resultSchema = JSON.parse(data.toString());
            }
        })
    }

    /**
     * Try to parse the player id. If it is not valid, it will log it and send a 400 Bad Request response.
     * If this returns null, stop processing the request and don't send anything.
     * @param req
     * @param res
     */
    private static parsePlayerID(req: Request, res: Response): PlayerID {
        let id: PlayerID = null;
        try {
            id = new PlayerID(req.query.player_id);
        } catch (e) {
            console.error("No valid player id provided %o", e);
            res.sendStatus(400);
        }
        return id;
    }
}