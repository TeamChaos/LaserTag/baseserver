# Base Server
NodeJS server program handling game configuration and evaluation. Communicates with ESPs as well as configuration browser

## Functionality
### Routes
- `/` frontend
- `/api` backend for webinterface and apps. All post data and responses are JSON documents containing either  
    - `result: 0` if successful
    - `result: 1` and `error:"message"`  if the request cannot be processed right now
    - `result: 2` and `error:"message"`  on bad request  
    - `result: 3` and `error:"message"`  on internal error  
    See schemas/api_response.schema.json. Additional properties may be included at top level
- `/esp` backend for the player master devices. Success is signalized via the HTTP status code
    - HTTP 204: Success - No data
    - HTTP 200: Success - JSON encoded data
    - HTTP 400: Bad request - Missing or illegal queries
    - HTTP 500: Internal server error
    - HTTP 503: Wrong time for this request - E.g. registrations while the game is running

## Notes
### Mumble Integration
This node application communicates with the mumble server (murmur) hosted separately (see docker-compose) via ICE which is a RCP protocol.  
The communication uses the available `ice` npm package which natively offers TypeScript support. 
To be able to interact with the Murmur API via Ice, the ICE API definition file [Murmur.Ice in the mumble repository](https://github.com/mumble-voip/mumble/blob/master/src/murmur/Murmur.ice) is converted using `slice2js` which supports TypeScript as well to generate `Murmur.js` as well as `Murmur.d.ts` 

## Project Setup
### Productive
- Make sure you have NodeJS (LTS10 dubnium) and npm installed
- Run `npm install` here
- Compile with `npm run grunt`
- Set the following environment variables:
  - `ESP_AUTH_USER` using the user flashed to the ESPs
  - `ESP_AUTH_SECRET` using the secret flashed to the ESPs
  - `APP_AUTH_SECRET_BASE` using the base secret used to generate the player app credentials
  - `WEB_PASSWORD` for the password required to control the webinterface
  - `NETWORK_TYPE` for the game network to use (`data` for data network communication or `radio` for 433MHz
    communication)
  - `EXTERNAL_HOST` using the external domain/ip this server can be reached
  - `EXTERNAL_PORT` using the external port this sever can be reached at
  - `EXTERNAL_IS_SECURE` whether the server can be reached via SSL: `true`/`false`
 - Start with `node bin/www`
 
 #### Voice Server
 If you want to manage a Mumble/Murmur voice server for the app, you will have to add further environment variables
- `VOICE_SERVER_ICE_IP` ip address the Murmur ICE interface can be reached directly
- `VOICE_SERVER_ICE_PORT` port the Murmur ICE interface is listening on
- `VOICE_SERVER_ICE_SECRET` the `icesecretwrite` of the ICE/Murmur server
- `VOICE_SERVER_HOST` address the voice server can be publicly reached at
- `VOICE_SERVER_PORT` port the voice server can be publicly reached at
- `VOICE_SERVER_PASSWORD` server password of the Murmur server

This will only work if the ICE/Murmur server can be directly reached from this instance, otherwise you may have to use a ZeroC Glacier2 as a proxy.

### Development
- Same as productive, but additionally set environment variable `DEBUG=true`. You can skip the secrets if you want
- Start e.g. with `NETWORK_TYPE=data DEBUG=true node bin/www`

### Debugging requests
In `docs/insomnia_workspace.yaml` you can find a set of requests you can use to debug the server.
Import them using [Insomnia](https://insomnia.rest). You can also copy the default local workspace to a (secret) workspace for the productive server including the necessary secrets.


## Deployment

### Docker
Build using the provided Dockerfile. Eg. `sudo docker build -t lasertag-baseserver Baseserver/`.
If desired, derive your docker-compose setup using the included example docker-compose.yml. It is designed to work behind a [Traefik](https://traefik.io/) proxy for TLS/SSL support and more.
If you want to use a voiceserver (included in the `docker-compose.yml`) you will also need to configure the Mumble Murmur part `/etc/mumble/config.ini`:
```
serverpassword=password
ice="tcp -h 172.16.238.3  -p 6502"
icesecretwrite=password
```

### Lazermaster Raspberry Setup
The Lazermaster Raspberry provides a hotspot and DHCP server to allow ESPs and other clients to connect to it.  
#### Properties
- Ip Address: `192.168.1.1`
- Hostname: `lazermaster`
- Mapped domain: `www.lasertag.maxgb.de` `lasertag.maxgb.de`

#### Setup
- Install raspbian stretch
- Setup both eth0 and wlan0, setup dnsmasq (with the properties above), setup hostapd (e.g. like [here](https://www.elektronik-kompendium.de/sites/raspberry-pi/2002171.htm), skip the routing part)
- Eth0 is optional but makes allows connecting from another network
- Add 
```
no-hosts
addn-hosts=/etc/dnsmasq.hosts
```
to `/etc/dnsmasq.conf`
- Add `192.168.1.1 www.lasertag.maxgb.de lasertag.maxgb.de` to `/etc/dnsmasq.hosts`
- Setup a reverse proxy (e.g. nginx) to serve the node server (port: 1984) via https on port 443.

##### Suggested nginx setup
Setup nginx with a self-signed certificate similar to here https://www.digitalocean.com/community/tutorials/how-to-create-a-self-signed-ssl-certificate-for-nginx-in-ubuntu-16-04
```
server {
        listen 80 default_server;
        listen [::]:80 default_server;
        listen 443 ssl http2 default_server;
        listen [::]:433 ssl http2 default_server;
        server_name lasertag.maxgb.de;

        include snippets/self-signed.conf;
        include snippets/ssl-params.conf;


        location / {
                proxy_pass http://localhost:1984;
        }

        location /ws_app {
                proxy_pass http://localhost:1984/ws_app;
                proxy_http_version 1.1;
                proxy_set_header Upgrade $http_upgrade;
                proxy_set_header Connection "upgrade";
                proxy_read_timeout 86400;
        }

}
```